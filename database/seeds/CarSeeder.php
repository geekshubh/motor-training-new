<?php

use Illuminate\Database\Seeder;
use App\Cars;
class CarSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      Cars::create([
        'name' => 'Honda City',
        'unavailable_on' => 'Wednesday',
        'instructor_id' => 4,
      ]);
      Cars::create([
        'name' => 'Wagon R',
        'unavailable_on' => 'Sunday',
        'instructor_id' => 5,
      ]);
    }
}
