<?php

use Illuminate\Database\Seeder;
use App\CarSchedule;
class CarScheduleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      CarSchedule::create([
        'instructor_id' => 4,
        'car_id'  =>  1,
        'start_time' => '06:00',
        'end_time'   => '06:30',
        'availability'  => 'Available',
      ]);
      CarSchedule::create([
        'instructor_id' => 4,
        'car_id'  =>  1,
        'start_time' => '06:30',
        'end_time'   => '07:00',
        'availability'  => 'Available',
      ]);
      CarSchedule::create([
        'instructor_id' => 4,
        'car_id'  =>  1,
        'start_time' => '07:00',
        'end_time'   => '07:30',
        'availability'  => 'Available',
      ]);
      CarSchedule::create([
        'instructor_id' => 4,
        'car_id'  =>  1,
        'start_time' => '07:30',
        'end_time'   => '08:00',
        'availability'  => 'Available',
      ]);
      CarSchedule::create([
        'instructor_id' => 4,
        'car_id'  =>  1,
        'start_time' => '08:00',
        'end_time'   => '08:30',
        'availability'  => 'Available',
      ]);
      CarSchedule::create([
        'instructor_id' => 4,
        'car_id'  =>  1,
        'start_time' => '08:30',
        'end_time'   => '09:00',
        'availability'  => 'Available',
      ]);
      CarSchedule::create([
        'instructor_id' => 4,
        'car_id'  =>  1,
        'start_time' => '09:00',
        'end_time'   => '09:30',
        'availability'  => 'Available',
      ]);
      CarSchedule::create([
        'instructor_id' => 4,
        'car_id'  =>  1,
        'start_time' => '09:30',
        'end_time'   => '10:00',
        'availability'  => 'Available',
      ]);
      CarSchedule::create([
        'instructor_id' => 4,
        'car_id'  =>  1,
        'start_time' => '10:00',
        'end_time'   => '10:30',
        'availability'  => 'Available',
      ]);
      CarSchedule::create([
        'instructor_id' => 4,
        'car_id'  =>  1,
        'start_time' => '10:30',
        'end_time'   => '11:00',
        'availability'  => 'Available',
      ]);
      CarSchedule::create([
        'instructor_id' => 4,
        'car_id'  =>  1,
        'start_time' => '11:00',
        'end_time'   => '11:30',
        'availability'  => 'Available',
      ]);
      CarSchedule::create([
        'instructor_id' => 4,
        'car_id'  =>  1,
        'start_time' => '11:30',
        'end_time'   => '12:00',
        'availability'  => 'Available',
      ]);
      CarSchedule::create([
        'instructor_id' => 4,
        'car_id'  =>  1,
        'start_time' => '12:00',
        'end_time'   => '12:30',
        'availability'  => 'Available',
      ]);
      CarSchedule::create([
        'instructor_id' => 4,
        'car_id'  =>  1,
        'start_time' => '15:30',
        'end_time'   => '16:00',
        'availability'  => 'Available',
      ]);
      CarSchedule::create([
        'instructor_id' => 4,
        'car_id'  =>  1,
        'start_time' => '16:00',
        'end_time'   => '16:30',
        'availability'  => 'Available',
      ]);
      CarSchedule::create([
        'instructor_id' => 4,
        'car_id'  =>  1,
        'start_time' => '16:30',
        'end_time'   => '17:00',
        'availability'  => 'Available',
      ]);
      CarSchedule::create([
        'instructor_id' => 4,
        'car_id'  =>  1,
        'start_time' => '17:00',
        'end_time'   => '17:30',
        'availability'  => 'Available',
      ]);
      CarSchedule::create([
        'instructor_id' => 4,
        'car_id'  =>  1,
        'start_time' => '17:30',
        'end_time'   => '18:00',
        'availability'  => 'Available',
      ]);
      CarSchedule::create([
        'instructor_id' => 4,
        'car_id'  =>  1,
        'start_time' => '18:00',
        'end_time'   => '18:30',
        'availability'  => 'Available',
      ]);
      CarSchedule::create([
        'instructor_id' => 4,
        'car_id'  =>  1,
        'start_time' => '18:30',
        'end_time'   => '19:00',
        'availability'  => 'Available',
      ]);
      CarSchedule::create([
        'instructor_id' => 4,
        'car_id'  =>  1,
        'start_time' => '19:00',
        'end_time'   => '19:30',
        'availability'  => 'Available',
      ]);
      // Instructor 2
      CarSchedule::create([
        'instructor_id' => 5,
        'car_id'  =>  2,
        'start_time' => '06:00',
        'end_time'   => '06:30',
        'availability'  => 'Available',
      ]);
      CarSchedule::create([
        'instructor_id' => 5,
        'car_id'  =>  2,
        'start_time' => '06:30',
        'end_time'   => '07:00',
        'availability'  => 'Available',
      ]);
      CarSchedule::create([
        'instructor_id' => 5,
        'car_id'  =>  2,
        'start_time' => '07:00',
        'end_time'   => '07:30',
        'availability'  => 'Available',
      ]);
      CarSchedule::create([
        'instructor_id' => 5,
        'car_id'  =>  2,
        'start_time' => '07:30',
        'end_time'   => '08:00',
        'availability'  => 'Available',
      ]);
      CarSchedule::create([
        'instructor_id' => 5,
        'car_id'  =>  2,
        'start_time' => '08:00',
        'end_time'   => '08:30',
        'availability'  => 'Available',
      ]);
      CarSchedule::create([
        'instructor_id' => 5,
        'car_id'  =>  2,
        'start_time' => '08:30',
        'end_time'   => '09:00',
        'availability'  => 'Available',
      ]);
      CarSchedule::create([
        'instructor_id' => 5,
        'car_id'  =>  2,
        'start_time' => '09:00',
        'end_time'   => '09:30',
        'availability'  => 'Available',
      ]);
      CarSchedule::create([
        'instructor_id' => 5,
        'car_id'  =>  2,
        'start_time' => '09:30',
        'end_time'   => '10:00',
        'availability'  => 'Available',
      ]);
      CarSchedule::create([
        'instructor_id' => 5,
        'car_id'  =>  2,
        'start_time' => '10:00',
        'end_time'   => '10:30',
        'availability'  => 'Available',
      ]);
      CarSchedule::create([
        'instructor_id' => 5,
        'car_id'  =>  2,
        'start_time' => '10:30',
        'end_time'   => '11:00',
        'availability'  => 'Available',
      ]);
      CarSchedule::create([
        'instructor_id' => 5,
        'car_id'  =>  2,
        'start_time' => '11:00',
        'end_time'   => '11:30',
        'availability'  => 'Available',
      ]);
      CarSchedule::create([
        'instructor_id' => 5,
        'car_id'  =>  2,
        'start_time' => '11:30',
        'end_time'   => '12:00',
        'availability'  => 'Available',
      ]);
      CarSchedule::create([
        'instructor_id' => 5,
        'car_id'  =>  2,
        'start_time' => '12:00',
        'end_time'   => '12:30',
        'availability'  => 'Available',
      ]);
      CarSchedule::create([
        'instructor_id' => 5,
        'car_id'  =>  2,
        'start_time' => '15:30',
        'end_time'   => '16:00',
        'availability'  => 'Available',
      ]);
      CarSchedule::create([
        'instructor_id' => 5,
        'car_id'  =>  2,
        'start_time' => '16:00',
        'end_time'   => '16:30',
        'availability'  => 'Available',
      ]);
      CarSchedule::create([
        'instructor_id' => 5,
        'car_id'  =>  2,
        'start_time' => '16:30',
        'end_time'   => '17:00',
        'availability'  => 'Available',
      ]);
      CarSchedule::create([
        'instructor_id' => 5,
        'car_id'  =>  2,
        'start_time' => '17:00',
        'end_time'   => '17:30',
        'availability'  => 'Available',
      ]);
      CarSchedule::create([
        'instructor_id' => 5,
        'car_id'  =>  2,
        'start_time' => '17:30',
        'end_time'   => '18:00',
        'availability'  => 'Available',
      ]);
      CarSchedule::create([
        'instructor_id' => 5,
        'car_id'  =>  2,
        'start_time' => '18:00',
        'end_time'   => '18:30',
        'availability'  => 'Available',
      ]);
      CarSchedule::create([
        'instructor_id' => 5,
        'car_id'  =>  2,
        'start_time' => '18:30',
        'end_time'   => '19:00',
        'availability'  => 'Available',
      ]);
      CarSchedule::create([
        'instructor_id' => 5,
        'car_id'  =>  2,
        'start_time' => '19:00',
        'end_time'   => '19:30',
        'availability'  => 'Available',
      ]);

    }
}
