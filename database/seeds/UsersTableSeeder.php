<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'email' => 'admin@admin.com',
            'name' => 'Kiran Murkar',
            'role_id' => 1,
            'password' => bcrypt('password')
        ]);

        User::create([
            'email' => 'instructor@admin.com',
            'name' => 'Shubh Jangam',
            'role_id' => 2,
            'password' => bcrypt('password')
        ]);

        User::create([
            'email' => 'student@admin.com',
            'name' => 'Harsh Modi',
            'role_id' => 3,
            'password' => bcrypt('password')
        ]);
        User::create([
            'email' => 'siddeshwar.mali@gmail.com',
            'name' => 'Siddeshwar Mali',
            'role_id' => 2,
            'password' => bcrypt('password')
        ]);
        User::create([
            'email' => 'ramesh.nigappa@gmail.com',
            'name' => 'Ramesh Nigappa',
            'role_id' => 2,
            'password' => bcrypt('password')
        ]);
    }
}
