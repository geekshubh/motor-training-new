<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarScheduleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('car_schedule', function (Blueprint $table) {
          $table->increments('id');
          $table->integer('instructor_id')->unsigned();
          $table->integer('car_id')->unsigned();
          $table->integer('booked_by')->unsigned()->nullable();
          $table->text('start_time')->nullable();
          $table->text('end_time')->nullable();
          $table->text('availability')->nullable();
          $table->foreign('instructor_id')->references('id')->on('users')->onDelete('cascade');
          $table->foreign('car_id')->references('id')->on('cars')->onDelete('cascade');
          $table->foreign('booked_by')->references('id')->on('users')->onDelete('cascade');
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('car_schedule');
    }
}
