<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
          $table->increments('id');
          $table->text('mid')->nullable();
          $table->text('txn_id')->nullable();
          $table->text('order_id')->nullable();
          $table->text('bank_txn_id')->nullable();
          $table->text('txn_amount')->nullable();
          $table->text('status')->nullable();
          $table->text('resp_code')->nullable();
          $table->text('resp_msg')->nullable();
          $table->text('txn_date')->nullable();
          $table->text('gateway_name')->nullable();
          $table->text('payment_mode')->nullable();
          $table->text('checksum_hash')->nullable();
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
