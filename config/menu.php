<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Admin Navigation Menu
    |--------------------------------------------------------------------------
    |
    | This array is for Navigation menus of the backend.  Just add/edit or
    | remove the elements from this array which will automatically change the
    | navigation.
    |
    */

    // SIDEBAR LAYOUT - MENU
    'sidebar-admin' =>[
      [
        'title' => 'Dashboard',
        'link' => '/',
        'active' => 'dashboard*',
        'icon' => 'icon-fa icon-fa-dashboard',
      ],
      [
        'title' => 'Vehicles',
        'link' => 'vehicles',
        'active' => 'vehicles*',
        'icon' => 'icon-fa icon-fa-dashboard',
      ],
      [
        'title' => 'Vehicle Schedule',
        'link' => 'vehicle-schedule',
        'active' => 'vehicles-schedule/*',
        'icon' => 'icon-fa icon-fa-dashboard',
      ],
      [
        'title' => 'Client Schedule',
        'link' => 'client-schedule',
        'active' => 'client-schedule/*',
        'icon' => 'icon-fa icon-fa-dashboard',
      ],
      [
        'title' => 'Users',
        'link' => 'users',
        'active' => 'users/*',
        'icon' => 'icon-fa icon-fa-dashboard',
      ],
      [
        'title' => 'Fees',
        'link' => 'fees',
        'active' => 'fees/*',
        'icon' => 'icon-fa icon-fa-dashboard',
      ],
      [
        'title' => 'Logout',
        'link' => 'logout',
        'active' => 'logout/*',
        'icon' => 'icon-fa icon-fa-dashboard',
      ],
    ],
    'sidebar-student' =>[
      [
        'title' => 'Dashboard',
        'link' => '/',
        'active' => 'dashboard*',
        'icon' => 'icon-fa icon-fa-dashboard',
      ],
      [
        'title' => 'Vehicle Schedule',
        'link' => 'vehicle-schedule',
        'active' => 'vehicles-schedule/*',
        'icon' => 'icon-fa icon-fa-dashboard',
      ],
      [
        'title' => 'Client Schedule',
        'link' => 'client-schedule',
        'active' => 'client-schedule/*',
        'icon' => 'icon-fa icon-fa-dashboard',
      ],
      [
        'title' => 'Fees',
        'link' => 'fees',
        'active' => 'fees/*',
        'icon' => 'icon-fa icon-fa-dashboard',
      ],
      [
        'title' => 'Logout',
        'link' => 'logout',
        'active' => 'logout/*',
        'icon' => 'icon-fa icon-fa-dashboard',
      ],
    ],
    'sidebar-instructor' =>[
      [
        'title' => 'Dashboard',
        'link' => '/',
        'active' => 'dashboard*',
        'icon' => 'icon-fa icon-fa-dashboard',
      ],
      [
        'title' => 'Vehicle Schedule',
        'link' => 'vehicle-schedule',
        'active' => 'vehicles-schedule/*',
        'icon' => 'icon-fa icon-fa-dashboard',
      ],
      [
        'title' => 'Client Schedule',
        'link' => 'client-schedule',
        'active' => 'client-schedule/*',
        'icon' => 'icon-fa icon-fa-dashboard',
      ],
      [
        'title' => 'Logout',
        'link' => 'logout',
        'active' => 'logout/*',
        'icon' => 'icon-fa icon-fa-dashboard',
      ],
    ],
];
