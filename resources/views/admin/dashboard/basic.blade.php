@extends('admin.layouts.layout-basic')
@section('scripts')
<script src="{{ url('assets/admin/js/users/users.js')}}"></script>
@stop
@section('content')
    <div class="main-content" id="dashboardPage">
        <div class="row">
            <div class="col-md-12 col-lg-6 col-xl-6">
                <a class="dashbox" href="#">
                    <i class="icon-fa icon-fa-envelope text-primary"></i>
                    <span class="title">
                      Sessions Scheduled
                    </span>
                    <span class="desc">
                      {{ $sessions_scheduled }}
                    </span>
                </a>
            </div>
            <div class="col-md-12 col-lg-6 col-xl-6">
                <a class="dashbox" href="#">
                    <i class="icon-fa icon-fa-ticket text-success"></i>
                    <span class="title">
                      Sessions Completed
                    </span>
                    <span class="desc">
                      {{ $sessions_completed }}
                    </span>
                </a>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-xl-6 mt-2">
                <div class="card">
                    <div class="card-header">
                        <h6><i class="icon-fa icon-fa-shopping-cart text-danger"></i>Today's Schedule</h6>
                    </div>
                    <div class="card-body">
                        <table id="schedule-datatable" class="table">
                            <thead>
                            <tr>
                                @if(Auth::user()->isAdmin())
                                <th class="all">Student Name</th>
                                <th class="desktop">Student Contact Number</th>
                                @endif
                                <th class="all">Timing</th>
                                @if(Auth::user()->isStudent())
                                <th class="desktop">Instructor Name</th>
                                <th class="desktop">Instructor Contact Number</th>
                                @endif
                                <th class="desktop">Options</th>
                            </tr>
                            </thead>
                            <tbody>
                              @if (count($sessions_scheduled_table) > 0)
                              @foreach ($sessions_scheduled_table as $schedule)
                              <tr>
                                @if(Auth::user()->isAdmin() || Auth::user()->isInstructor())
                                <td>{{ $schedule->client_schedule->name }}</td>
                                <td>{{ $schedule->client_schedule->phone_number }}</td>
                                @endif
                                <td>{{ $schedule->start_time }} - {{ $schedule->end_time }}</td>
                                @if(Auth::user()->isStudent())
                                <td>{{ $schedule->client_schedule_instructor->name }}</td>
                                <td>{{ $schedule->client_schedule_instructor->phone_number }}</td>
                                @endif
                                <td>
                                  <a href="{{ route('client-schedule.show',[$schedule->id])}}" class="btn btn-md mr-2 mb-2 mt-2 btn-info">Show</a>
                                  @if(Auth::user()->isAdmin() || Auth::user()->isInstructor())
                                  <a href="{{ route('client-schedule.edit',[$schedule->id])}}" class="btn btn-md mr-2 mb-2 mt-2 btn-info">Edit</a>
                                  @endif
                                </td>
                              </tr>
                              @endforeach
                              @else
                              <tr>
                                <td colspan="7">@lang('translate.no_entries')</td>
                              </tr>
                              @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-lg-12 col-xl-6 mt-2">
                <div class="card">
                    <div class="card-header">
                        <h6><i class="icon-fa icon-fa-shopping-cart text-danger"></i>Fees</h6>
                    </div>
                    <div class="card-body">
                        <table id="fees-datatable">
                          <thead>
                            <tr>
                              <th class="desktop">Title</th>
                              <th class="all">Client</th>
                              <th class="desktop">Amount</th>
                              <th class="all">Due Date</th>
                              <th class="desktop">Status</th>
                              <th class="desktop">Actions</th>
                            </tr>
                          </thead>
                          <tbody>
                          @foreach($fees as $fees)
                          <tr>
                            <td>{{$fees->title}}</td>
                            <td>{{$fees->fees->name}}</td>
                            <td>{{$fees->amount}}</td>
                            <td>{{ $fees->due_date }}</td>
                            <td>{{$fees->status}}</td>
                            <td>
                              @if(Auth::user()->isAdmin())
                              <a href="{{ route('fees.edit',[$fees->id])}}" class="btn btn-md btn-info">Edit</a>
                              {!! Form::open(array('style' => 'display: inline-block;','method' => 'DELETE','onsubmit' => "return confirm('Are you Sure');",'route' => ['users.destroy', $fees->id])) !!}
                              {!! Form::submit('Delete', array('class' => 'btn btn-md mr-2 mb-2 mt-2 btn-danger')) !!}
                              {!! Form::close() !!}
                              @endif
                              @if(Auth::user()->isStudent() && ($fees->status == 'Unpaid' || $fees->status == 'unpaid'))
                                <a href="{{ route('fees.pay',[$fees->id]) }}" class="btn btn-md btn-info"> Pay Fees</a>
                              @endif
                            </td>
                          </tr>
                          @endforeach
                          </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        @if(Auth::user()->isStudent())
        <div class="row">
          <div class="col-lg-12 col-xl-6 mt-2">
          </div>
          <div class="col-lg-12 col-xl-6 mt-2">
              <div class="card">
                  <div class="card-header">
                      <h6>User Details</h6>
                  </div>
                  <div class="card-body">
                      <table class="table">
                        <thead>
                          <tr>
                            <th>Learning License No.</th>
                            <th>Learning License Issue Date</th>
                            <th>Learning License Validity</th>
                            <th>Tentative Test Date</th>
                          </tr>
                        </thead>
                        <tbody>
                        <tr>
                          <td>{{Auth::user()->ll_no}}</td>
                          <td>{{Auth::user()->ll_issue_date}}</td>
                          <td>{{Auth::user()->ll_expiry_date}}</td>
                          <td>{{ Auth::user()->driving_test_date }}</td>
                        </tr>
                        </tbody>
                      </table>
                  </div>
              </div>
          </div>
        </div>
        @endif
    </div>
@stop
