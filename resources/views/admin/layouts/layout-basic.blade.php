<!DOCTYPE html>
<html>
<head>
    <title>Radhe Krishna Motor Training School</title>
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,300,700' rel='stylesheet' type='text/css'>
    <script src="{{asset('assets/admin/js/core/pace.js')}}"></script>
    <link href="{{ asset(mix('assets/admin/css/laraspace.css')) }}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.9.0/css/all.css" integrity="sha256-PF6MatZtiJ8/c9O9HQ8uSUXr++R9KBYu4gbNG5511WE=" crossorigin="anonymous" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.9.0/js/all.js" integrity="sha256-S1WJaJce5DQaJRMMO9igZPd6WRgObuRlQXE8i7yKOd8=" crossorigin="anonymous"></script>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    @include('admin.layouts.partials.favicons')
    @yield('styles')
    @toastr_css
</head>
<body class="layout-default skin-default">
    @include('admin.layouts.partials.laraspace-notifs')

    <div id="app" class="site-wrapper">
        @include('admin.layouts.partials.header')
        <div class="mobile-menu-overlay"></div>
        @include('admin.layouts.partials.sidebar',['type' => 'default'])
        @yield('content')

        @include('admin.layouts.partials.footer')

    </div>

    <script src="{{asset(mix('assets/admin/js/core/plugins.js'))}}"></script>
    <script src="{{asset('assets/admin/js/demo/skintools.js')}}"></script>
    <script src="{{asset(mix('assets/admin/js/core/app.js'))}}"></script>

    @yield('scripts')
    @toastr_js
    @toastr_render
</body>
</html>
