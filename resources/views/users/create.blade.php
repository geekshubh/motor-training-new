@extends('admin.layouts.layout-basic')
@section('content')
<div class="main-content">
  <div class="page-header">
    <h3 class="page-title">Add User</h3>
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="{{ route('admin.dashboard')}}">Home</a></li>
      <li class="breadcrumb-item"><a href="{{ route('users.index')}}">Users</a></li>
      <li class="breadcrumb-item active">Add User</li>
    </ol>
  </div>
  <div class="row">
    <div class="col-sm-12">
      <div class="card">
        <div class="card-header">
          <h6>
            Add User
            <a href="{{ route('users.index')}}" class="btn btn-md btn-danger float-right"><i class="icon-fa icon-fa-arrow-circle-left"></i>Back</a>
          </h6>
        </div>
        <div class="card-body">
          {!! Form::open(['method' => 'POST', 'route' => ['users.store'] ,'enctype'=>'multipart/form-data']) !!}
          <div class="form-group row">
            <label for="email1" class="col-sm-2 col-form-label">Email</label>
            <div class="col-sm-10">
              <input type="email" name="email" class="form-control" id="email1" placeholder="Email">
            </div>
          </div>
          <div class="form-group row">
            <label for="name1" class="col-sm-2 col-form-label">Name</label>
            <div class="col-sm-10">
              <input type="text" name="name" class="form-control" id="name1" placeholder="Full Name">
            </div>
          </div>
          <div class="form-group row">
            <label for="number1" class="col-sm-2 col-form-label">Phone Number</label>
            <div class="col-sm-10">
              <input type="text" name="phone_number" class="form-control" id="number1" placeholder="Phone Number">
            </div>
          </div>
          <div class="form-group row">
            <label for="password1" class="col-sm-2 col-form-label">Password</label>
            <div class="col-sm-10">
              <input type="password" name="password" class="form-control" id="password1" placeholder="Password">
            </div>
          </div>
          <div class="form-group row">
            <label for="ll_no" class="col-sm-2 col-form-label">Learning License No.</label>
            <div class="col-sm-10">
              <input type="text" name="ll_no" class="form-control" id="ll_no" placeholder="Learning License Number">
            </div>
          </div>
          <div class="form-group row">
            <label for="issue_date" class="col-sm-2 col-form-label">Learning License Issue Date</label>
            <div class="col-sm-10">
              <div class="input-group">
                  <div class="input-group-prepend">
                      <span class="input-group-text">
                      <i class="icon-fa icon-fa-calendar"></i>
                      </span>
                  </div>
                  <input type="text" autocomplete="false" name="ll_issue_date" id="issue_date" class="form-control ls-datepicker" data-autoclose="true" data-format="d-m-Y" >
              </div>
            </div>
          </div>
          <div class="form-group row">
            <label for="expiry_date" class="col-sm-2 col-form-label">Learning License Expiry Date</label>
            <div class="col-sm-10">
              <div class="input-group">
                  <div class="input-group-prepend">
                      <span class="input-group-text">
                      <i class="icon-fa icon-fa-calendar"></i>
                      </span>
                  </div>
                  <input type="text" autocomplete="false" name="ll_expiry_date" id="expiry_date" class="form-control ls-datepicker" data-autoclose="true" data-format="d-m-Y" >
              </div>
            </div>
          </div>
          <div class="form-group row">
            <label for="tentative_date" class="col-sm-2 col-form-label">Tentative Test Date</label>
            <div class="col-sm-10">
              <div class="input-group">
                  <div class="input-group-prepend">
                      <span class="input-group-text">
                      <i class="icon-fa icon-fa-calendar"></i>
                      </span>
                  </div>
                  <input type="text" autocomplete="false" name="driving_test_date" id="tentaive_date" class="form-control ls-datepicker" data-autoclose="true" data-format="d-m-Y" >
              </div>
            </div>
          </div>
          <div class="form-group">
              <label for="exampleFormControlSelect1">Select Role</label>
              {!! Form::select('role_id', $roles, old('role_id'), ['class' => 'ls-select2 form-control' ,'id'=>'exampleFormControlSelect1']) !!}
          </div>
          <button type = "submit" class="btn btn-md btn-success">Save</button>
        </div>
      </div>
    </div>
  </div>
</div>
@stop
