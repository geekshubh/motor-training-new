@extends('admin.layouts.layout-basic')
@section('scripts')
<script src="{{ url('assets/admin/js/users/users.js')}}"></script>
@stop
@section('content')
<div class="main-content">
  <div class="page-header">
    <h3 class="page-title">Users</h3>
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="{{ route('admin.dashboard')}}">Home</a></li>
      <li class="breadcrumb-item active">Users</li>
    </ol>
    <div class="page-actions">
    </div>
  </div>
  <div class="row">
    <div class="col-sm-12">
      <div class="card">
        <div class="card-header">
          <h6>{{ $user->name }}
            <a href="{{ route('users.index')}}" class="btn btn-primary btn-md float-right"><i class="icon-fa icon-fa-plus"></i>Back to List</a>
          </h6>
        </div>
        <div class="card-body">
          <table class="table table-bordered table-striped">
            <tr>
              <th>Name</th>
              <td>{{ $user->name }}</td>
            </tr>
            <tr>
              <th>Phone Number</th>
              <td>{{ $user->phone_number }}</td>
            </tr>
            <tr>
              <th>Learning License Number</th>
              <td>{{ $user->ll_no }}</td>
            </tr>
            <tr>
              <th>Learning License Validity</th>
              <td>{{ $user->ll_issue_date }} - {{ $user->ll_expiry_date }}</td>
            </tr>
            <tr>
              <th>Tentative Driving Test Date</th>
              <td>{{ $user->driving_test_date }}</td>
            </tr>
            <tr>
              <th>Sessions Scheduled</th>
              <td>{{ $scheduled }}</td>
            </tr>
            <tr>
              <th>Sessions Completed</th>
              <td>{{ $completed }}</td>
            </tr>
            <tr>
              <th>Start Date</th>
              <td>{{ $start_date }}</td>
            </tr>
            <tr>
              <th>End Date</th>
              <td>{{ $end_date }}</td>
            </tr>
          </table>
          <div class="row">
            <div class="col-sm-12">
              <div class="card">
                <div class="card-header">
                  <h6>Schedule</h6>
                </div>
                <div class="card-body">
                  <table id="schedule-datatable" class="table table-striped dt-responsive table-bordered " data-order="[]" cellspacing="0" width="100%">
                    <thead>
                      <tr>
                        @if(Auth::user()->isAdmin())
                        <th>Car Name</th>
                        <th>Instructor Name</th>
                        @endif
                        <th>Title</th>
                        <th>Time</th>
                        <th>Date</th>
                        <th>Session Completed</th>
                      </tr>
                    </thead>
                    @if (count($schedule) > 0)
                    @foreach ($schedule as $schedule)
                    <tr>
                      @if(Auth::user()->isAdmin())
                      <td>{{ $schedule->client_schedule_car->name }}</td>
                      <td>{{ $schedule->client_schedule_instructor->name }}</td>
                      @endif
                      <td>{{ $schedule->title }}</td>
                      <td>{{ $schedule->start_time }} - {{ $schedule->end_time }}</td>
                      <td>{{ $schedule->date->format('d-m-Y') }}</td>
                      @if(!empty($schedule->instructor_login_time))
                      <td>Yes</td>
                      @else
                      <td>No</td>
                      @endif
                    </tr>
                    @endforeach
                    @else
                    <tr>
                      <td colspan="7">@lang('translate.no_entries')</td>
                    </tr>
                    @endif
                    <tbody>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
          <h3>Fees</h3>
          <table id="users-datatable" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
              <tr>
                <th>Title</th>
                <th>Client</th>
                <th>Amount</th>
                <th>Due Date</th>
                <th>Status</th>
                <th>Actions</th>
              </tr>
            </thead>
            @foreach($fees as $fees)
            <tr>
              <td>{{$fees->title}}</td>
              <td>{{$fees->fees->name}}</td>
              <td>{{$fees->amount}}</td>
              <td>{{ $fees->due_date }}</td>
              <td>{{$fees->status}}</td>
              <td>
                <a href="{{ route('fees.edit',[$fees->id])}}" class="btn btn-md btn-info">Edit</a>
                {!! Form::open(array('style' => 'display: inline-block;','method' => 'DELETE','onsubmit' => "return confirm('Are you Sure');",'route' => ['users.destroy', $fees->id])) !!}
                {!! Form::submit('Delete', array('class' => 'btn btn-md mr-2 mb-2 mt-2 btn-danger')) !!}
                {!! Form::close() !!}
                @if(Auth::user()->isStudent() && ($fees->status == 'Unpaid' || $fees->status == 'unpaid'))
                  <a href="{{ route('fees.pay',[$fees->id]) }}" class="btn btn-md btn-info"> Pay Fees</a>
                @endif
                @if($fees->status == 'Paid')
                <a href="{{ route('fees.generate-invoice',[$fees->id]) }}" class="btn btn-md btn-info">Download Invoice</a>
                @endif
              </td>
            </tr>
            @endforeach
            <tbody>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
@stop
