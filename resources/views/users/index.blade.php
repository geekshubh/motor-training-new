@extends('admin.layouts.layout-basic')
@section('scripts')
<script src="{{ url('assets/admin/js/users/users.js')}}"></script>
@stop
@section('content')
<div class="main-content">
  <div class="page-header">
    <h3 class="page-title">Users</h3>
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="{{ route('admin.dashboard')}}">Home</a></li>
      <li class="breadcrumb-item active">Users</li>
    </ol>
    <div class="page-actions">
    </div>
  </div>
  <div class="row">
    <div class="col-sm-12">
      <div class="card">
        <div class="card-header">
          <h6>All Users
            <a href="{{ route('users.add')}}" class="btn btn-primary btn-md float-right"><i class="icon-fa icon-fa-plus"></i></a>
          </h6>
          <div class="card-actions">
          </div>
        </div>
        <div class="card-body">
          <table id="users-datatable" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
              <tr>
                <th>Name</th>
                <th>Email</th>
                <th>Role</th>
                <th>Registered On</th>
                <th>Actions</th>
              </tr>
            </thead>
            @foreach($users as $user)
            <tr>
              <td>{{$user->name}}</td>
              <td>{{$user->email}}</td>
              <td>{{$user->role_id}}</td>
              <td>{{$user->created_at}}</td>
              <td>
                <a href="{{ route('users.edit',[$user->id])}}" class="btn btn-md btn-info">Edit</a>
                {!! Form::open(array('style' => 'display: inline-block;','method' => 'DELETE','onsubmit' => "return confirm('Are you Sure');",'route' => ['users.destroy', $user->id])) !!}
                {!! Form::submit('Delete', array('class' => 'btn btn-md mr-2 mb-2 mt-2 btn-danger')) !!}
                {!! Form::close() !!}
                <a href="{{ route('users.show',[$user->id])}}" class="btn btn-info btn-md">View</a>
              </td>
            </tr>
            @endforeach
            <tbody>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
@stop
