@extends('admin.layouts.layout-basic')
@section('content')
<div class="main-content">
  <div class="page-header">
    <h3 class="page-title">Add Fee</h3>
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="{{ route('admin.dashboard')}}">Home</a></li>
      <li class="breadcrumb-item"><a href="{{ route('fees.index')}}">Fees</a></li>
      <li class="breadcrumb-item active">Add Fees</li>
    </ol>
  </div>
  <div class="row">
    <div class="col-sm-12">
      <div class="card">
        <div class="card-header">
          <h6>
            Add Fees
            <a href="{{ route('users.index')}}" class="btn btn-md btn-danger float-right"><i class="icon-fa icon-fa-arrow-circle-left"></i>Back</a>
          </h6>
        </div>
        <div class="card-body">
          {!! Form::open(['method' => 'POST', 'route' => ['fees.store'] ,'enctype'=>'multipart/form-data']) !!}
          <div class="form-group row">
            <label for="title1" class="col-sm-2 col-form-label">Title</label>
            <div class="col-sm-10">
              <input type="text" name="title" class="form-control" id="title1" placeholder="Title">
            </div>
          </div>
          <div class="form-group row">
            <label for="description" class="col-sm-2 col-form-label">Description</label>
            <div class="col-sm-10">
              <input type="text" name="description" class="form-control" id="description" placeholder="Description">
            </div>
          </div>

          <div class="form-group row">
            <label for="amount" class="col-sm-2 col-form-label">Amount</label>
            <div class="col-sm-10">
              <input type="number" name="amount" class="form-control" id="amount" placeholder="amount">
            </div>
          </div>
          <div class="form-group row">
            <label for="date" class="col-sm-2 col-form-label">Due Date</label>
            <div class="col-sm-10">
              <div class="input-group">
                  <div class="input-group-prepend">
                      <span class="input-group-text">
                      <i class="icon-fa icon-fa-calendar"></i>
                      </span>
                  </div>
                  <input type="text" autocomplete="false" name="due_date" id="date" class="form-control ls-datepicker" data-autoclose="true" data-format="d-m-Y" >
              </div>
            </div>
          </div>
          <div class="form-group row">
            <label for="balance" class="col-sm-2 col-form-label">Balance</label>
            <div class="col-sm-10">
              <input type="number" name="balance" class="form-control" id="amount" placeholder="balance">
            </div>
          </div>
          <div class="form-group">
              <label for="exampleFormControlSelect1">Select Client</label>
              {!! Form::select('client_id', $clients, old('client_id'), ['class' => 'ls-select2 form-control' ,'id'=>'exampleFormControlSelect1']) !!}
          </div>
          <div class="form-group row">
            <label for="status" class="col-sm-2 col-form-label">Status</label>
            <div class="col-sm-10">
              <input type="text" name="status" class="form-control" id="status" placeholder="Paid/Unpaid">
            </div>
          </div>
          <button type = "submit" class="btn btn-md btn-success">Save</button>
        </div>
      </div>
    </div>
  </div>
</div>
@stop
