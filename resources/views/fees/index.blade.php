@extends('admin.layouts.layout-basic')
@section('scripts')
<script src="{{ url('assets/admin/js/users/users.js')}}"></script>
@stop
@section('content')
<div class="main-content">
  <div class="page-header">
    <h3 class="page-title">Fees</h3>
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="{{ route('admin.dashboard')}}">Home</a></li>
      <li class="breadcrumb-item active">Fees</li>
    </ol>
    <div class="page-actions">
    </div>
  </div>
  <div class="row">
    <div class="col-sm-12">
      <div class="card">
        <div class="card-header">
          <h6>All Fees
            <a href="{{ route('fees.create')}}" class="btn btn-primary btn-md float-right"><i class="icon-fa icon-fa-plus"></i></a>
          </h6>
          <div class="card-actions">
          </div>
        </div>
        <div class="card-body">
          <table id="users-datatable" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
              <tr>
                <th>Title</th>
                <th>Client</th>
                <th>Amount</th>
                <th>Due Date</th>
                <th>Status</th>
                <th>Actions</th>
              </tr>
            </thead>
            @foreach($fees as $fees)
            <tr>
              <td>{{$fees->title}}</td>
              <td>{{$fees->fees->name}}</td>
              <td>{{$fees->amount}}</td>
              <td>{{ $fees->due_date }}</td>
              <td>{{$fees->status}}</td>
              <td>
                @if(Auth::user()->isAdmin())
                <a href="{{ route('fees.edit',[$fees->id])}}" class="btn btn-md btn-info">Edit</a>
                {!! Form::open(array('style' => 'display: inline-block;','method' => 'DELETE','onsubmit' => "return confirm('Are you Sure');",'route' => ['users.destroy', $fees->id])) !!}
                {!! Form::submit('Delete', array('class' => 'btn btn-md mr-2 mb-2 mt-2 btn-danger')) !!}
                {!! Form::close() !!}
                @endif
                @if(Auth::user()->isStudent() && ($fees->status == 'Unpaid' || $fees->status == 'unpaid'))
                  <a href="{{ route('fees.pay',[$fees->id]) }}" class="btn btn-md btn-info"> Pay Fees</a>
                @endif
                @if($fees->status == 'Paid')
                <a href="{{ route('fees.generate-invoice',[$fees->id]) }}" class="btn btn-md btn-info">Download Invoice</a>
                @endif
              </td>
            </tr>
            @endforeach
            <tbody>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
@stop
