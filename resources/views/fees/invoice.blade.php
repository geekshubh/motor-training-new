<!doctype html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <title>Invoice - {{ $fees->installment_unique_id }}</title>
    <style type="text/css">
      @page {
      margin: 0px;
      }
      body {
      margin: 0px;
      }
      * {
      font-family: CenturyGothic, AppleGothic; font-size: 13px; font-style: normal;
      }
      .invoice p
      {
          font-size:20px;
      }
      a {
      color: #fff;
      text-decoration: none;
      }
      table {
      font-size: x-small;
      }
      tfoot tr td {
      font-weight: bold;
      font-size: x-small;
      }
      .invoice table {
      margin: 15px;
      }
      .invoice h3 {
      margin-left: 15px;
      }
      .information {
      background-color: #800000;
      color: #FFF;
      }
      .information .logo {
      margin: 5px;
      }
      .information table {
      padding: 10px;
      }
    </style>
  </head>
  <body>
    <div class="information">
      <table width="100%">
        <tr>
          <td align="left" style="width: 40%;">
            <h3></h3>
            <pre>
                  Name : {{ $fees->fees->name }}
                  <br />
                  Email: {{ $fees->fees->email }}
                  <br />
                  Number: {{ $fees->fees->phone_number }}
                  <br />
                  Date: {{ $fees->created_at }}
                  <br />
                  Identifier: {{$fees->installment_unique_id}}
                  <br />
                  Status: {{ $fees->status }}
                </pre>
          </td>
          <td align="center">
            <img src="{{ url('assets/admin/img/logo-login.png')}}" alt="Logo" width="128" class="logo"/>
          </td>
          <td align="right" style="width: 40%;">
            <h3>RADHE KRISHNA MOTOR TRAINING SCHOOL</h3>
            <pre>
              Website: https://radhekrishnamts.com
              <br />
              Email :- radhekrishnamts@gmail.com
              <br />
              Contact No. :- 8425864685/6/7 / 02240100040
              <br />
              Shop No.13, Panchsheel Residency,
              <br />
              Mahavir Nagar, Kandivali West,
              <br />
              Mumbai-400067
              <br />
            </pre>
          </td>
        </tr>
      </table>
    </div>
    <br/>
    <div class="invoice">
      <h3>Unique ID : {{ $fees->installment_unique_id }}</h3>
      <p style="margin:50px;">Received the amount of Rs.{{ $fees->amount }}/- for full time/part time training course on {{ $car_name }}
      Balance amount Rs {{ $fees->balance }}</p>

      <p align="right" style="margin-right:100px;"><img src ="{{ url('assets/signature-3.jpg')}}"><br>Signed By: Harshad Mayekar</p>
      <br />
      <br />
      <p style="margin-left:10px;">Terms & Conditions :</p>
      <p style="margin:50px;">
      1) Fees once paid are non refundable and non transferable.
      <br>
      2) Learning license charges are exclusive of the course fees
      </p>
    </div>
    <div class="information" style="position: absolute; bottom: 0;">
      <table width="100%">
        <tr>
          <td align="left" style="width: 50%;">
            &copy; {{ date('Y') }} {{ config('app.url') }} - All rights reserved.
          </td>
          <td align="right" style="width: 50%;">
            POWERED BY TRUST...DRIVEN BY VALUES!!!
          </td>
        </tr>
      </table>

    </div>
  </body>
</html>
