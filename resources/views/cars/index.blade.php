@extends('admin.layouts.layout-basic')
@section('scripts')
<script src="{{ url('assets/admin/js/users/users.js')}}"></script>
@stop
@section('content')
<div class="main-content">
  <div class="page-header">
    <h3 class="page-title">Vehicles</h3>
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="{{ route('admin.dashboard')}}">Home</a></li>
      <li class="breadcrumb-item active">Vehicles</li>
    </ol>
    <div class="page-actions">
    </div>
  </div>
  <div class="row">
    <div class="col-sm-12">
      <div class="card">
        <div class="card-header">
          <h6>All Vehicles
            <a href="{{ route('cars.add')}}" class="btn btn-primary btn-md float-right"><i class="icon-fa icon-fa-plus"></i></a>
          </h6>
          <div class="card-actions">
          </div>
        </div>
        <div class="card-body">
          <table id="users-datatable" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
              <tr>
                <th>Name</th>
                <th>Instructor</th>
                <th>Unavailable On</th>
                <th>Actions</th>
              </tr>
            </thead>
            @foreach($cars as $car)
            <tr>
              <td>{{$car->name}}</td>
              <td>{{$car->car_instructor->name}}</td>
              <td>{{$car->unavailable_on}}</td>
              <td>
                <a href="{{ route('cars.edit',[$car->id])}}" class="btn btn-md btn-info">Edit</a>
                {!! Form::open(array('style' => 'display: inline-block;','method' => 'DELETE','onsubmit' => "return confirm('Are you Sure');",'route' => ['cars.destroy', $car->id])) !!}
                {!! Form::submit('Delete', array('class' => 'btn btn-md mr-2 mb-2 mt-2 btn-danger')) !!}
                {!! Form::close() !!}
              </td>
            </tr>
            @endforeach
            <tbody>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
@stop
