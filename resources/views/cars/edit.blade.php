@extends('admin.layouts.layout-basic')
@section('content')
<div class="main-content">
  <div class="page-header">
    <h3 class="page-title">Vehicles</h3>
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="{{ route('admin.dashboard')}}">Home</a></li>
      <li class="breadcrumb-item"><a href="{{ route('cars.index')}}">Vehicles</a></li>
      <li class="breadcrumb-item active">Edit Vehicle</li>
    </ol>
    <div class="page-actions">
    </div>
  </div>
  <div class="row">
    <div class="col-sm-12">
      <div class="card">
        <div class="card-header">
          <h6>
            Edit Vehicle
            <a href="{{ route('cars.index')}}" class="btn btn-md btn-danger float-right"><i class="icon-fa icon-fa-arrow-circle-left"></i>Back</a>
          </h6>
        </div>
        <div class="card-body">
          {!! Form::model($cars,['method' => 'PUT', 'route' => ['cars.update',$cars->id] ,'enctype'=>'multipart/form-data']) !!}
          <div class="form-group row">
            <label for="name1" class="col-sm-2 col-form-label">Name</label>
            <div class="col-sm-10">
              <input type="text" name="name" class="form-control" id="name1" placeholder="Full Name" value = "{{ $cars->name }}">
            </div>
          </div>
          <div class="form-group">
              <label for="exampleFormControlSelect1">Select Instructor</label>
              {!! Form::select('instructor_id', $instructors, old('instructor_id'), ['class' => 'ls-select2 form-control' ,'id'=>'exampleFormControlSelect1']) !!}
          </div>
          <div class="form-group">
              <label for="exampleFormControlSelect2">Unavailable on</label>
              {!! Form::select('unavailable_on', $days, old('unavailable_on'), ['class' => 'ls-select2 form-control' ,'id'=>'exampleFormControlSelect2']) !!}
          </div>
          <button type = "submit" class="btn btn-md btn-success">Save</button>
        </div>
      </div>
    </div>
  </div>
</div>
@stop
