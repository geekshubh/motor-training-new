@extends('admin.layouts.layout-basic')
@section('scripts')
<script src="{{ url('assets/admin/js/users/users.js')}}"></script>
@stop
@section('content')
<div class="main-content">
  <div class="page-header">
    <h3 class="page-title">Vehicle Schedule</h3>
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="{{ route('admin.dashboard')}}">Home</a></li>
      <li class="breadcrumb-item active">Vehicle Schedule</li>
    </ol>
    <div class="page-actions">
    </div>
  </div>
  <div class="row">
    <div class="col-sm-12">
      <div class="card">
        <div class="card-header">
          <h6>All Vehicles
            <a href="{{ route('car-schedule.add')}}" class="btn btn-primary btn-md float-right"><i class="icon-fa icon-fa-plus"></i></a>
          </h6>
          <div class="card-actions">
          </div>
        </div>
        <div class="card-body">
          <table id="users-datatable" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
              <tr>
                <th>Car Name</th>
                <th>Actions</th>
              </tr>
            </thead>
            @foreach($schedule as $schedule)
            <tr>
              <td>{{ $schedule->car_schedule_car->name }}</td>
              <td>
                <a href="{{ route('car-schedule.view',[$schedule->car_id])}}" class="btn btn-md btn-info">View Schedule</a>
              </td>
            </tr>
            @endforeach
            <tbody>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
@stop
