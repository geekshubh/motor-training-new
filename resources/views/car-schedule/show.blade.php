@extends('admin.layouts.layout-basic')
@section('scripts')
<script src="{{ url('assets/admin/js/users/users.js')}}"></script>
@stop
@section('content')
<div class="main-content">
  <div class="page-header">
    <h3 class="page-title">Vehicle Schedule</h3>
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="{{ route('admin.dashboard')}}">Home</a></li>
      <li class="breadcrumb-item active">Vehicle Schedule</li>
    </ol>
    <div class="page-actions">
    </div>
  </div>
  <div class="row">
    <div class="col-sm-12">
      <div class="card">
        <div class="card-header">
          <h6>Vehicle Schedule
          </h6>
          <div class="card-actions">
          </div>
        </div>
        <div class="card-body">
          <table id="users-datatable" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
              <tr>
                <th>Start Time</th>
                <th>End Time</th>
                <th>Options</th>
              </tr>
            </thead>
            @if (count($schedule) > 0)
            @foreach ($schedule as $schedule)
            <tr>
              <td>{{ $schedule->start_time }}</td>
              <td>{{ $schedule->end_time}}</td>
              <td>
                @if(Auth::user()->isStudent())
                  @if(empty($schedule->booked_by))
                  <a type="button" class="btn btn-primary" href="{{ route('client-schedule.book_appointment',[$schedule->id]) }}">Book an Appointment</button>
                  @elseif(($schedule->availability == 'Unavailable'))
                  This Slot would be available after {{ $schedule->booked_till }}
                  @endif
                @endif
                @if(Auth::user()->isAdmin())
                  <a href="{{ route('car-schedule.edit',[$schedule->id]) }}" class="btn btn-md mr-2 mb-2 mt-2 btn-info">Edit</a>
                  {!! Form::open(array('style' => 'display: inline-block;','method' => 'DELETE','onsubmit' => "return confirm('".trans("translate.are_you_sure")."');",'route' => ['car-schedule.destroy', $schedule->id])) !!}
                  {!! Form::submit('Delete', array('class' => 'btn btn-md mr-2 mb-2 mt-2 btn-danger')) !!}
                  {!! Form::close() !!}
                @endif
                @if(Auth::user()->isInstructor())
                  @if(!empty($schedule->booked_by))
                  <h3>Booked by {{ $schedule->car_schedule_booked->name }}</h3>
                  @else
                  <h3>Empty</h3>
                  @endif
                @endif
              </td>
            </tr>
            @endforeach
            @else
            <tr>
              <td colspan="7">@lang('translate.no_entries')</td>
            </tr>
            @endif
            <tbody>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
@stop
