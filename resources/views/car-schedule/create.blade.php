@extends('admin.layouts.layout-basic')
@section('content')
<div class="main-content">
  <div class="page-header">
    <h3 class="page-title">Vehicle Schedule</h3>
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="{{ route('admin.dashboard')}}">Home</a></li>
      <li class="breadcrumb-item"><a href="{{ route('car-schedule.index')}}">Vehicle Schedule</a></li>
      <li class="breadcrumb-item active">Add Vehicle Schedule</li>
    </ol>
    <div class="page-actions">
    </div>
  </div>
  <div class="row">
    <div class="col-sm-12">
      <div class="card">
        <div class="card-header">
          <h6>
            Add Vehicle Schedule
            <a href="{{ route('car-schedule.index')}}" class="btn btn-md btn-danger float-right"><i class="icon-fa icon-fa-arrow-circle-left"></i>Back</a>
          </h6>
        </div>
        <div class="card-body">
          {!! Form::open(['method' => 'POST', 'route' => ['car-schedule.store'] ,'enctype'=>'multipart/form-data']) !!}
          <div class="form-group">
              <label for="exampleFormControlSelect1">Select Car</label>
              {!! Form::select('car_id', $cars, old('car_id'), ['class' => 'ls-select2 form-control' ,'id'=>'exampleFormControlSelect1']) !!}
          </div>
          <div class="form-group row">
            <label for="start_time_1" class="col-sm-2 col-form-label">Start Time</label>
            <div class="col-sm-10">
              <div class="input-group">
                  <div class="input-group-prepend">
                      <span class="input-group-text">
                      <i class="icon-fa icon-fa-calendar"></i>
                      </span>
                  </div>
                  <input type="text" name="start_time" id="start_time_1" class="form-control ls-clockpicker" data-autoclose="true" autocomplete="false">
              </div>
            </div>
          </div>
          <div class="form-group row">
            <label for="end_time_1" class="col-sm-2 col-form-label">End Time</label>
            <div class="col-sm-10">
              <div class="input-group">
                  <div class="input-group-prepend">
                      <span class="input-group-text">
                      <i class="icon-fa icon-fa-calendar"></i>
                      </span>
                  </div>
                  <input type="text" autocomplete="false" name="end_time" id="end_time_1" class="form-control ls-clockpicker" data-autoclose="true">
              </div>
            </div>
          </div>
          <div class="form-group">
              <label for="exampleFormControlSelect2">Availablity</label>
              {!! Form::select('availability', $availability, old('availability'), ['class' => 'ls-select2 form-control' ,'id'=>'exampleFormControlSelect2']) !!}
          </div>
          <button type = "submit" class="btn btn-md btn-success">Save</button>
        </div>
      </div>
    </div>
  </div>
</div>
@stop
