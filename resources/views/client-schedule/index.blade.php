@extends('admin.layouts.layout-basic')
@section('scripts')
<script src="{{ url('assets/admin/js/users/users.js')}}"></script>
@stop
@section('content')
<div class="main-content">
  <div class="page-header">
    <h3 class="page-title">Client Schedule</h3>
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="{{ route('admin.dashboard')}}">Home</a></li>
      <li class="breadcrumb-item active">Client Schedule</li>
    </ol>
    <div class="page-actions">
    </div>
  </div>
  <div class="row">
    <div class="col-sm-12">
      <div class="card">
        <div class="card-header">
          <h6>Schedule</h6>
          <div class="card-actions">
            @if(Auth::user()->isInstructor())
            <a href="{{ route('client-schedule-tomorrow.index')}}" class="btn btn-md btn-info float-right mr-2 mt-2">Tomorrow</a>
            <a href="{{ route('client-schedule-yesterday.index')}}" class="btn btn-md btn-info float-right mr-2 mt-2">Yesterday</a>
            <a href="{{ route('client-schedule.index')}}" class="btn btn-md btn-info float-right mr-2 mt-2">Today</a>
            @endif
          </div>
        </div>
        <div class="card-body">
          <table id="users-datatable" class="table table-striped dt-responsive table-bordered " data-order="[]" cellspacing="0" width="100%">
            <thead>
              <tr>
                @if(Auth::user()->isAdmin())
                <th>Car Name</th>
                <th>Instructor Name</th>
                @endif
                @if(Auth::user()->isAdmin() || Auth::user()->isInstructor())
                <th>Client Name</th>
                @endif
                <th>Title</th>
                <th>Time</th>
                <th>Date</th>
                <th>Options</th>
              </tr>
            </thead>
            @if (count($schedule) > 0)
            @foreach ($schedule as $schedule)
            <tr>
              @if(Auth::user()->isAdmin())
              <td>{{ $schedule->client_schedule_car->name }}</td>
              <td>{{ $schedule->client_schedule_instructor->name }}</td>
              @endif
              @if(Auth::user()->isAdmin() || Auth::user()->isInstructor())
              <td><a href="{{ route('users.show',[$schedule->client_schedule->id]) }}">{{ $schedule->client_schedule->name }}</a></td>
              @endif
              <td>{{ $schedule->title }}</td>
              <td>{{ $schedule->start_time }} - {{ $schedule->end_time }}</td>
              <td>{{ $schedule->date->format('d-m-Y') }}</td>
              <td>
                @if((Auth::user()->isInstructor() || Auth::user()->isAdmin()) && (empty($schedule->instructor_login_time)))
                  <a href="{{ route('client-schedule.instructor_login',[$schedule->id])}}" class="btn btn-md mr-2 mb-2 mt-2 btn-info">Instructor Login</a>
                @endif
                @if((Auth::user()->isStudent()) && ((!empty($schedule->instructor_login_time)) && (empty($schedule->client_login_time))))
                  <a href="{{ route('client-schedule.student_login',[$schedule->id])}}" class="btn btn-md mr-2 mb-2 mt-2 btn-info">Student Login</a>
                @endif

                @if(((Auth::user()->isInstructor()) || Auth::user()->isAdmin()) && ((!empty($schedule->instructor_login_time)) && (empty($schedule->instructor_logout_time))))
                  <a href="{{ route('client-schedule.instructor_logout',[$schedule->id])}}" class="btn btn-md mr-2 mb-2 mt-2 btn-info"> Instructor Logout</a>
                @endif

                @if((Auth::user()->isStudent()) && ((!empty($schedule->instructor_login_time)) && (!empty($schedule->client_login_time)) && (empty($schedule->client_logout_time))))
                  <a href="{{ route('client-schedule.student_logout',[$schedule->id])}}" class="btn btn-md mr-2 mb-2 mt-2 btn-info">Student Logout</a>
                @endif
                <a href="{{ route('client-schedule.show',[$schedule->id])}}" class="btn btn-md mr-2 mb-2 mt-2 btn-info">Show</a>
                @if(Auth::user()->isAdmin() || Auth::user()->isInstructor())
                <a href="{{ route('client-schedule.edit',[$schedule->id])}}" class="btn btn-md mr-2 mb-2 mt-2 btn-info">Edit</a>
                <a href="{{ route('client-schedule.destroy',[$schedule->id])}}" class="btn btn-md mr-2 mb-2 mt-2 btn-info">Delete</a>
                @endif
              </td>
            </tr>
            @endforeach
            @else
            <tr>
              <td colspan="7">@lang('translate.no_entries')</td>
            </tr>
            @endif
            <tbody>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
@stop
