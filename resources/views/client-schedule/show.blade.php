@extends('admin.layouts.layout-basic')
@section('scripts')
    <script src="/assets/admin/js/users/users.js"></script>
@stop
@section('content')
<div class="main-content">
  <div class="page-header">
    <h3 class="page-title">Client Schedule</h3>
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="{{ route('admin.dashboard')}}">Home</a></li>
      <li class="breadcrumb-item"><a href="{{ route('car-schedule.index')}}">Client Schedule</a></li>
      <li class="breadcrumb-item active">View Client Schedule</li>
    </ol>
    <div class="page-actions">
    </div>
  </div>
  <div class="row">
    <div class="col-sm-12">
      <div class="card">
        <div class="card-header">
          <h6>
            View Client Schedule
            <a href="{{ route('client-schedule.index')}}" class="btn btn-md btn-danger float-right"><i class="icon-fa icon-fa-arrow-circle-left"></i>Back</a>
          </h6>
        </div>
        <div class="card-body">
          <div class="row">
            <div class ="col-sm-12 col-md-12 col-lg-12">
              <table class="table table-user-information">
                <tbody>
                  <tr>
                    <td>Car</td>
                    <td>{{ $schedule->client_schedule_car->name }}</td>
                  </tr>
                  <tr>
                    <td>Instructor</td>
                    <td>{{ $schedule->client_schedule_instructor->name }}</td>
                  </tr>
                  <tr>
                    <td>Client</td>
                    <td>{{ $schedule->client_schedule->name }}</td>
                  </tr>
                  <tr>
                    <td>Date</td>
                    <td>{{ $schedule->date->format('d-m-Y') }}</td>
                  </tr>
                  <tr>
                    <td>Start Time</td>
                    <td>{{ $schedule->start_time }}</td>
                  </tr>
                  <tr>
                    <td>End Time</td>
                    <td>{{ $schedule->end_time }}</td>
                  </tr>
                  <tr>
                    <td>Instructor Login Time</td>
                    <td>{{ $schedule->instructor_login_time }}</td>
                  </tr>
                  <tr>
                    <td>Student Login Time</td>
                    <td>{{ $schedule->client_login_time }}</td>
                  </tr>
                  <tr>
                    <td>Student Logout Time</td>
                    <td>{{ $schedule->client_logout_time }}</td>
                  </tr>
                  <tr>
                    <td>Instructor Logout Time</td>
                    <td>{{ $schedule->instructor_logout_time }}</td>
                  </tr>
                  <tr>
                    <td>Instructor Total Time</td>
                    <td>{{ $schedule->instructor_total_time }}</td>
                  </tr>
                  <tr>
                    <td>Student Total Time</td>
                    <td>{{ $schedule->client_total_time }}</td>
                  </tr>
                  <tr>
                    <td>Average Time</td>
                    <td>{{ $schedule->average_time }}</td>
                  </tr>
                  <tr>
                    <td>User Details</td>
                    <td><a href="{{ route('users.show',[$schedule->client_schedule->id]) }}" class="btn btn-default btn-success">Show User</a></td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@stop
