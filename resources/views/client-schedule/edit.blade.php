@extends('admin.layouts.layout-basic')
@section('content')
<div class="main-content">
  <div class="page-header">
    <h3 class="page-title">Client Schedule</h3>
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="{{ route('admin.dashboard')}}">Home</a></li>
      <li class="breadcrumb-item"><a href="{{ route('car-schedule.index')}}">Client Schedule</a></li>
      <li class="breadcrumb-item active">Edit Client Schedule</li>
    </ol>
    <div class="page-actions">
    </div>
  </div>
  <div class="row">
    <div class="col-sm-12">
      <div class="card">
        <div class="card-header">
          <h6>
            Edit Client Schedule
            <a href="{{ route('client-schedule.index')}}" class="btn btn-md btn-danger float-right"><i class="icon-fa icon-fa-arrow-circle-left"></i>Back</a>
          </h6>
        </div>
        <div class="card-body">
          {!! Form::model($schedule,['method' => 'PUT', 'route' => ['client-schedule.update',$schedule->id] ,'enctype'=>'multipart/form-data']) !!}
          <div class="form-group row">
            <label for="date" class="col-sm-2 col-form-label">Date</label>
            <div class="col-sm-10">
              <div class="input-group">
                  <div class="input-group-prepend">
                      <span class="input-group-text">
                      <i class="icon-fa icon-fa-calendar"></i>
                      </span>
                  </div>
                  <input type="text" autocomplete="false" name="date" id="date" class="form-control ls-datepicker" data-autoclose="true" value = "{{ $schedule->date }}" data-format="d-m-Y" >
              </div>
            </div>
          </div>
          <div class="form-group row">
            <label for="start_time_1" class="col-sm-2 col-form-label">Start Time</label>
            <div class="col-sm-10">
              <div class="input-group">
                  <div class="input-group-prepend">
                      <span class="input-group-text">
                      <i class="icon-fa icon-fa-calendar"></i>
                      </span>
                  </div>
                  <input type="text" name="start_time" id="start_time_1" class="form-control ls-clockpicker" data-autoclose="true" autocomplete="false" value = "{{ $schedule->start_time }}">
              </div>
            </div>
          </div>
          <div class="form-group row">
            <label for="end_time_1" class="col-sm-2 col-form-label">End Time</label>
            <div class="col-sm-10">
              <div class="input-group">
                  <div class="input-group-prepend">
                      <span class="input-group-text">
                      <i class="icon-fa icon-fa-calendar"></i>
                      </span>
                  </div>
                  <input type="text" autocomplete="false" name="end_time" id="end_time_1" class="form-control ls-clockpicker" data-autoclose="true" value = "{{ $schedule->end_time }}">
              </div>
            </div>
          </div>
          <button type = "submit" class="btn btn-md btn-success">Update</button>
        </div>
      </div>
    </div>
  </div>
</div>
@stop
