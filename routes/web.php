<?php

/*
|--------------------------------------------------------------------------
| Frontend Routes
|--------------------------------------------------------------------------
| Define the routes for your Frontend pages here
|
*/


/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
| Route group for Backend prefixed with "admin".
| To Enable Authentication just remove the comment from Admin Middleware
|
*/

Route::group(['middleware' => 'auth'], function () {

    // Dashboard
    //----------------------------------

    Route::get('/', [
        'as' => 'admin.dashboard', 'uses' => 'DashboardController@index'
    ]);

    Route::get('/dashboard/ecommerce', [
        'as' => 'admin.dashboard.ecommerce', 'uses' => 'DashboardController@ecommerce'
    ]);

    Route::get('/dashboard/finance', [
        'as' => 'admin.dashboard.finance', 'uses' => 'DashboardController@finance'
    ]);

    Route::get('users',['uses'=>'UsersController@index','as'=>'users.index']);
    Route::get('users/add',['uses'=>'UsersController@add','as'=>'users.add']);
    Route::post('users/store',['uses'=>'UsersController@store','as'=>'users.store']);
    Route::get('users/{id}/edit',['uses'=>'UsersController@edit','as'=>'users.edit']);
    Route::put('users/{id}/update',['uses'=>'UsersController@update','as'=>'users.update']);
    Route::delete('users/{id}/delete',['uses'=>'UsersController@destroy','as'=>'users.destroy']);
    Route::get('users/{id}',['uses'=>'UsersController@show','as'=>'users.show']);

    Route::get('vehicles',['uses'=>'CarsController@index','as'=>'cars.index']);
    Route::get('vehicles/add',['uses'=>'CarsController@create','as'=>'cars.add']);
    Route::post('vehicles/store',['uses'=>'CarsController@store','as'=>'cars.store']);
    Route::get('vehicles/{id}/edit',['uses'=>'CarsController@edit','as'=>'cars.edit']);
    Route::put('vehicles/{id}/update',['uses'=>'CarsController@update','as'=>'cars.update']);
    Route::delete('vehicles/{id}/destroy',['uses'=>'CarsController@destroy','as'=>'cars.destroy']);


    Route::get('vehicle-schedule',['uses'=>'CarScheduleController@index','as'=>'car-schedule.index']);
    Route::get('vehicle-schedule/add',['uses'=>'CarScheduleController@create','as'=>'car-schedule.add']);
    Route::post('vehicle-schedule/store',['uses'=>'CarScheduleController@store','as'=>'car-schedule.store']);
    Route::get('vehicle-schedule/{id}/edit',['uses'=>'CarScheduleController@edit','as'=>'car-schedule.edit']);
    Route::put('vehicle-schedule/{id}/update',['uses'=>'CarScheduleController@update','as'=>'car-schedule.update']);
    Route::delete('vehicle-schedule/{id}/destroy',['uses'=>'CarScheduleController@destroy','as'=>'car-schedule.destroy']);
    Route::get('vehicle-schedule/{car_id}/view',['uses'=>'CarScheduleController@show','as'=>'car-schedule.view']);

    Route::get('client-schedule/{schedule_id}/book_appointment',['uses'=>'ClientScheduleController@book_appointment','as'=>'client-schedule.book_appointment']);
    Route::get('client-schedule',['uses'=>'ClientScheduleController@index','as'=>'client-schedule.index']);
    Route::get('client-schedule/tomorrow',['uses'=>'ClientScheduleController@tomorrow_schedule','as'=>'client-schedule-tomorrow.index']);
    Route::get('client-schedule/yesterday',['uses'=>'ClientScheduleController@yesterday_schedule','as'=>'client-schedule-yesterday.index']);
    Route::get('client-schedule/{id}/instructor_login',['uses'=>'ClientScheduleController@instructor_login','as'=>'client-schedule.instructor_login']);
    Route::get('client-schedule/{id}/student_login',['uses'=>'ClientScheduleController@student_login','as'=>'client-schedule.student_login']);
    Route::get('client-schedule/{id}/student_logout',['uses'=>'ClientScheduleController@student_logout','as'=>'client-schedule.student_logout']);
    Route::get('client-schedule/{id}/instructor_logout',['uses'=>'ClientScheduleController@instructor_logout','as'=>'client-schedule.instructor_logout']);
    Route::get('client-schedule/{id}/show',['uses'=>'ClientScheduleController@show','as'=>'client-schedule.show']);
    Route::get('client-schedule/{id}/edit',['uses'=>'ClientScheduleController@edit','as'=>'client-schedule.edit']);
    Route::put('client-schedule/{id}/update',['uses'=>'ClientScheduleController@update','as'=>'client-schedule.update']);
    Route::get('client-schedule/{id}/destroy',['uses'=>'ClientScheduleController@destroy','as'=>'client-schedule.destroy']);
    //Route::resource('users', 'UsersController');

    Route::get('fees',['uses'=>'FeesController@index','as'=>'fees.index']);
    Route::get('fees/add',['uses'=>'FeesController@create','as'=>'fees.create']);
    Route::post('fees/store',['uses'=>'FeesController@store','as'=>'fees.store']);
    Route::get('fees/{id}/edit',['uses'=>'FeesController@edit','as'=>'fees.edit']);
    Route::put('fees/{id}/update',['uses'=>'FeesController@update','as'=>'fees.update']);
    Route::delete('fees/{id}/delete',['uses'=>'FeesController@destroy','as'=>'fees.destroy']);
    Route::get('fees/{id}/show',['uses'=>'FeesController@show','as'=>'fees.show']);

    Route::get('fees/{id}/pay',['uses'=>'PaymentsController@order','as'=>'fees.pay']);
    Route::post('payment/status',['uses'=>'PaymentsController@paymentCallback','as'=>'payment.callback']);
    Route::get('fees/{id}/generate-invoice',['uses'=>'FeesController@generateInvoice','as'=>'fees.generate-invoice']);


});
/*
|--------------------------------------------------------------------------
| Guest Routes
|--------------------------------------------------------------------------
| Guest routes cannot be accessed if the user is already logged in.
| He will be redirected to '/" if he's logged in.
|
*/

Route::group(['middleware' => ['guest']], function () {

    Route::get('login', [
        'as' => 'login', 'uses' => 'AuthController@login'
    ]);

    Route::get('register', [
        'as' => 'register', 'uses' => 'AuthController@register'
    ]);

    Route::post('login', [
        'as' => 'login.post', 'uses' => 'AuthController@postLogin'
    ]);

    Route::get('forgot-password', [
        'as' => 'forgot-password.index', 'uses' => 'ForgotPasswordController@getEmail'
    ]);

    Route::post('/forgot-password', [
        'as' => 'send-reset-link', 'uses' => 'ForgotPasswordController@postEmail'
    ]);

    Route::get('/password/reset/{token}', [
        'as' => 'password.reset', 'uses' => 'ForgotPasswordController@GetReset'
    ]);

    Route::post('/password/reset', [
        'as' => 'reset.password.post', 'uses' => 'ForgotPasswordController@postReset'
    ]);

    Route::get('auth/{provider}', 'AuthController@redirectToProvider');

    Route::get('auth/{provider}/callback', 'AuthController@handleProviderCallback');
});

Route::get('logout', [
    'as' => 'logout', 'uses' => 'AuthController@logout'
]);
