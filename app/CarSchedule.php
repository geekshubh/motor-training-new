<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CarSchedule extends Model
{
  protected $table = 'car_schedule';
  protected $fillable = ['instructor_id','car_id','booked_by','start_time','end_time','availability'];

  public function car_schedule_instructor()
  {
    return $this->belongsTo('App\User','instructor_id');
  }

  public function car_schedule_booked()
  {
    return $this->belongsTo('App\User','booked_by');
  }

  public function car_schedule_car()
  {
    return $this->belongsTo('App\Cars','car_id');
  }
}
