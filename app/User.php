<?php
namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Hash;
class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name','phone_number', 'email', 'password', 'll_no','ll_issue_date','ll_expiry_date','driving_test_date'
    ];

    public function setPasswordAttribute($input)
    {
      if ($input)
      {
        $this->attributes['password'] = app('hash')->needsRehash($input) ? Hash::make($input) : $input;
      }
    }

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    public static function login($request)
    {
        $remember = $request->remember;
        $email = $request->email;
        $password = $request->password;
        return (\Auth::attempt(['email' => $email, 'password' => $password], $remember));
    }

    public function role()
    {
      return $this->belongsTo('App\Role', 'role_id')->withTrashed();
    }

    public function car_instructor()
    {
      return $this->hasMany('App\Cars','instructor_id')->withTrashed();
    }
    public function client_schedule()
    {
      return $this->hasMany('App\ClientSchedule','client_id');
    }

    public function car_schedule_booked()
    {
      return $this->hasMany('App\CarSchedule','booked_by');
    }
    public function client_schedule_instructor()
    {
      return $this->hasMany('App\ClientSchedule','instructor_id');
    }

    public function isAdmin()
    {
      foreach ($this->role()->get() as $role)
      {
        if ($role->id == 1)
        {
          return true;
        }
      }
      return false;
    }

    public function isStudent()
    {
      foreach ($this->role()->get() as $role)
      {
        if ($role->id == 3)
        {
          return true;
        }
      }
      return false;
    }

    public function isInstructor()
    {
      foreach ($this->role()->get() as $role)
      {
        if ($role->id == 2)
        {
          return true;
        }
      }
      return false;
    }


}
