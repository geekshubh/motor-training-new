<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cars extends Model
{
  protected $table = 'cars';
  protected $fillable = ['name','description','instructor_id'];

  public function car_instructor()
  {
    return $this->belongsTo('App\User','instructor_id');
  }

  public function client_schedule_car()
  {
    return $this->hasMany('App\ClientSchedule','car_id');
  }
}
