<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
class Fees extends Model
{
  protected $fillable = ['title','description','client_id','amount','discount','balance','total_amount','installment_unique_id','status','due_date'];
  protected $table = "fees";

  public function fees()
  {
    return $this->belongsTo('App\User','client_id');
  }
}
