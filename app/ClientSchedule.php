<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClientSchedule extends Model
{
  protected $fillable = ['schedule_id','car_id','instructor_id','client_id','start_time','date','end_time','client_login_time','instructor_login_time','client_logout_time','instructor_logout_time','instructor_total_time','client_total_time','average_time'];
  protected $table = "client_schedule";

  public function client_schedule()
  {
    return $this->belongsTo('App\User','client_id');
  }

  public function client_schedule_car()
  {
    return $this->belongsTo('App\Cars','car_id');
  }
  public function client_schedule_instructor()
  {
    return $this->belongsTo('App\User','instructor_id');
  }
  protected $dates = ['date'];
}
