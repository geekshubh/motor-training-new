<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PaytmWallet;
use App\Fees;
use Auth;

class PaymentsController extends Controller
{
  public function order($id)
    {
        $payment = PaytmWallet::with('receive');
        $installment = Fees::where('id',$id)->first();
        $payment->prepare([
          'order' => $installment->installment_unique_id,
          'user' => Auth::id(),
          'mobile_number' => Auth::user()->phone_number,
          'email' => Auth::user()->email,
          'amount' => $installment->amount,
          'callback_url' => 'https://rkmt.e-ducate.in/payment/status'
        ]);
        return $payment->receive();
    }

    /**
     * Obtain the payment information.
     *
     * @return Object
     */
    public function paymentCallback()
    {
        $transaction = PaytmWallet::with('receive');
        $response = $transaction->response();
        if($transaction->isSuccessful())
        {
          $installment_unique_id = $transaction->getOrderId();
          $fees = Fees::where('installment_unique_id',$installment_unique_id)->update(['status'=>'Paid']);
          //$fees->status = 'Paid';
          //$fees->update();
          toastr()->success('Fees Paid Successfully','Success!');
          return redirect()->route('fees.index');
        }
        else if($transaction->isFailed())
        {
          toastr()->warning('Fees Transcation Failed','Failure!');
          return redirect()->route('fees.index');
        }
        else if($transaction->isOpen())
        {
          $failure = 'processing';
          dd($failure);
        }
        else
        {
          dd($transaction);
        }
    }
}
