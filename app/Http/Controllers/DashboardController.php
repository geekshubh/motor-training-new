<?php
namespace App\Http\Controllers;
use App\ClientSchedule;
use Carbon\Carbon;
use Auth;
use App\Fees;
class DashboardController extends Controller
{
    public function index()
    {
        if(Auth::user()->isAdmin())
        {
          $sessions_scheduled = ClientSchedule::where('date',Carbon::today())->count();
          $sessions_scheduled_table = ClientSchedule::where('date',Carbon::today())->get();
          $sessions_completed = ClientSchedule::where('date',Carbon::today())->whereNotNull('average_time')->count();
          $fees= Fees::all();
          return view('admin.dashboard.basic',compact('sessions_scheduled','sessions_completed','sessions_scheduled_table','fees'));
        }
        elseif(Auth::user()->isStudent())
        {
          $sessions_scheduled = ClientSchedule::where('client_id',Auth::id())->count();
          $sessions_scheduled_table = ClientSchedule::where('client_id',Auth::id())->get();
          $sessions_completed = ClientSchedule::where('client_id',Auth::id())->whereNotNull('average_time')->count();
          $fees = Fees::where('client_id',Auth::id())->get();
          return view('admin.dashboard.basic',compact('sessions_scheduled','sessions_completed','sessions_scheduled_table','fees'));
        }
        elseif(Auth::user()->isInstructor())
        {
          $sessions_scheduled = ClientSchedule::where('date',Carbon::today())->where('instructor_id',Auth::id())->count();
          $sessions_scheduled_table = ClientSchedule::where('date',Carbon::today())->where('instructor_id',Auth::id())->get();
          $sessions_completed = ClientSchedule::where('date',Carbon::today())->where('instructor_id',Auth::id())->whereNotNull('average_time')->count();
          $fees = Fees::where('due_date',Carbon::today())->get();
          return view('admin.dashboard.basic',compact('sessions_scheduled','sessions_completed','sessions_scheduled_table','fees'));
        }
    }

    public function basic()
    {
         return view('admin.dashboard.basic');
    }

    public function ecommerce()
    {
        return view('admin.dashboard.ecommerce');
    }

    public function finance()
    {
        return view('admin.dashboard.finance');
    }
}
