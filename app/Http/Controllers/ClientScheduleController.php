<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\CarSchedule;
use App\Cars;
use Auth;
use App\ClientSchedule;
use Carbon\Carbon;
class ClientScheduleController extends Controller
{
  public function index()
  {
    if(Auth::user()->isAdmin())
    {
      $schedule = ClientSchedule::all()->sortBy('date');
      $date = Carbon::today();
      return view('client-schedule.index',compact('schedule','date'));
    }
    elseif(Auth::user()->isStudent())
    {
      $schedule = ClientSchedule::where('client_id',Auth::id())->get()->sortBy('date');
      return view('client-schedule.index',compact('schedule'));
    }
    elseif(Auth::user()->isInstructor())
    {
      //$currentDate = Carbon::now();
      $date = Carbon::today();
      //$startDate = $currentDate->copy()->startOfWeek()->format('Y-m-d');
      //$endDate = $currentDate->copy()->endOfWeek()->format('Y-m-d');
      $schedule = ClientSchedule::where('instructor_id',Auth::id())->where('date',$date)->get();
      //dd($schedule);
      return view('client-schedule.index',compact('schedule','date'));
    }
    else
    {
      toastr()->warning('You are not allowed to access Client Schedule','Warning!');
      return redirect()->back();
    }
  }

  public function tomorrow_schedule()
  {
    if(Auth::user()->isInstructor())
    {
      $date = Carbon::tomorrow();
      $schedule = ClientSchedule::where('instructor_id',Auth::id())->where('date',$date)->get();
      return view('client-schedule.index',compact('schedule','date'));
    }
    else
    {
      toastr()->warning('You are not allowed to access Client Schedule','Warning!');
      return redirect()->back();
    }
  }

  public function yesterday_schedule()
  {
    if(Auth::user()->isInstructor())
    {
      $date = Carbon::yesterday();
      $schedule = ClientSchedule::where('instructor_id',Auth::id())->where('date',$date)->get();
      return view('client-schedule.index',compact('schedule','date'));
    }
    else
    {
      toastr()->warning('You are not allowed to access Client Schedule','Warning!');
      return redirect()->back();
    }
  }

  public function book_appointment($schedule_id)
  {
    if(Auth::user()->isStudent())
    {
      $schedule = CarSchedule::where('id',$schedule_id)->first();
      $start_time = $schedule->start_time;
      $end_time = $schedule->end_time;
      $instructor_id = $schedule->instructor_id;
      $car_id = $schedule->car_id;
      $car_day_off = Cars::where('id',$car_id)->pluck('unavailable_on')->first();
      $date = Carbon::now();
      $current_time = $date->format('H:i');
      //dd($current_time);
      /*if($current_time > $start_time)
      {
        $greater = 'Greater';
        dd($greater);
      }
      else
      {
        $greater = 'NOT GREATER';
        dd($greater);
      }*/
     $i = 0;
     if($current_time > $start_time)
     {
       $new_date = $date->addDays(1);
     }
     else
     {
       $new_date = $date;
     }
     while($i < 23)
     {
       $formatted_date = $new_date->format('Y-m-d');
       $check_day = $new_date->format('l');
       if($check_day != $car_day_off)
       {
         $k = $i + 1 ;
         $client_schedule = new ClientSchedule;
         $client_schedule->schedule_id = $schedule_id;
         $client_schedule->car_id = $car_id;
         $client_schedule->instructor_id = $instructor_id;
         $client_schedule->start_time = $start_time;
         $client_schedule->end_time = $end_time;
         $client_schedule->date = $formatted_date;
         $client_schedule->title = 'Day '.$k;
         $client_schedule->client_id = Auth::id();
         $client_schedule->save();
         $i++;
       }
       $new_date = $date->addDays(1);
     }

      /*$i = 1;

      while($i < 23)
      {
        $formatted_date = $new_date->format('Y-m-d');
        $check_day = $new_date->format('l');
        if($check_day != $car_day_off)
        {
          $client_schedule = new ClientSchedule;
          $client_schedule->schedule_id = $schedule_id;
          $client_schedule->car_id = $car_id;
          $client_schedule->instructor_id = $instructor_id;
          if($i = 1)
          {
            $client_schedule->title = 'Day '.$i.' Theoretical Introduction';
          }
          elseif($i = 22)
          {
            $client_schedule->title = 'Day '.$i.' Maintenance Session';
          }
          elseif($i = 23)
          {
            $client_schedule->title = 'Day '.$i.' Mock Test';
          }
          else
          {
            $client_schedule->title = 'Day '.$i. ' Driving Training';
          }
          $client_schedule->start_time = $start_time;
          $client_schedule->end_time = $end_time;
          $client_schedule->date = $formatted_date;
          $client_schedule->client_id = Auth::id();
          $client_schedule->save();
          $i++;
        }
        $new_date = $date->addDays(1);
      }*/
      $schedule = CarSchedule::findOrFail($schedule_id);
      $schedule->booked_by = Auth::id();
      $schedule->availability = "Unavailable";
      $last_date_query = ClientSchedule::where('schedule_id',$schedule_id)->where('car_id',$car_id)->orderBy('date','desc')->first();
      $last_date = $last_date_query->date;
      $schedule->booked_till = $last_date;
      $schedule->update();
      toastr()->success('Schedule Added Successfully','Success!');
      return redirect()->route('client-schedule.index');
    }
    else
    {
      toastr()->warning('You are not allowed to book an appointment','Warning!');
      return redirect()->back();
    }
  }

  public function edit($id)
  {
    if(Auth::user()->isAdmin())
    {
      $schedule = ClientSchedule::findOrFail($id);
      if(empty($schedule->average_time))
      {
        return view('client-schedule.edit',compact('schedule'));
      }
      else
      {
        toastr()->warning('Session Already Compeleted','Warning!');
        return redirect()->back();
      }
    }
    else
    {
      toastr()->warning('You are not allowed to edit the session','Warning!');
      return redirect()->back();
    }
  }

  public function update(Request $request,$id)
  {
    if(Auth::user()->isAdmin())
    {
      $client_schedule = ClientSchedule::findOrFail($id);
      $client_schedule->date = Carbon::parse($request->date)->format('Y-m-d');
      $client_schedule->start_time = $request->start_time;
      $client_schedule->end_time = $request->end_time;
      $client_schedule->update();
      toastr()->success('Schedule Updated Successfully ','Success!');
      return redirect()->route('client-schedule.index');
    }
    else
    {
      toastr()->warning('You are not allowed to update the session','Warning!');
      return redirect()->back();
    }
  }

  public function destroy($id)
  {
    if(Auth::user()->isAdmin())
    {
      $client_schedule = ClientSchedule::findOrFail($id);
      $client_schedule->delete();
      toastr()->success('Session Deleted Successfully','Success!');
      return redirect()->route('client-schedule.index');
    }
    else
    {
      toastr()->warning('You are not allowed to delete the session','Warning!');
      return redirect()->back();
    }
  }

  public function instructor_login($id)
  {
    if(Auth::user()->isAdmin() || Auth::user()->isInstructor())
    {
      $client_schedule = ClientSchedule::findOrFail($id);
      $time = Carbon::now();
      $start_time = Carbon::parse($client_schedule->start_time);
      $difference = $time->diffInMinutes($start_time);
      $date = Carbon::today();
      $schedule_date = $client_schedule->date;
      $instructor_login = $client_schedule->instructor_login_time;
      if(empty($instructor_login))
      {
        $client_schedule->instructor_login_time = Carbon::now()->format('d-m-Y H:i');
        $client_schedule->date = Carbon::now()->format('Y-m-d');
        $schedule_id = $client_schedule->schedule_id;
        $day_no = ClientSchedule::where('schedule_id',$schedule_id)->whereNotNull('instructor_login_time')->count();
        $day_no_new = $day_no+1;
        //dd($day_no);
        $client_schedule->title = 'Day '.$day_no_new;
        //dd($client_schedule->title);
        $client_schedule->update();
        toastr()->success('Instructor Logged In, Please tell the Student to login now','Success!');
        return redirect()->route('client-schedule.index');
      }
      else
      {
        toastr()->warning('Instructor Logged In Already','Warning!');
        return redirect()->back();
      }
    }
    else
    {
      toastr()->warning('You are not allowed to start the session as an Instructor','Warning!');
      return redirect()->back();
    }
  }

  public function student_login($id)
  {
    if(Auth::user()->isStudent())
    {
      $client_schedule = ClientSchedule::findOrFail($id);
      $client_login = $client_schedule->client_login_time;
      $instructor_login = $client_schedule->instructor_login_time;
      if(empty($client_login))
      {
        $client_schedule->client_login_time = Carbon::now()->format('d-m-Y H:i');
        $client_schedule->update();
        toastr()->success('Student session started successfully','Success!');
        return redirect()->route('client-schedule.index');
      }
      elseif(!empty($client_login))
      {
        toastr()->warning('You have already started the session','Warning!');
        return redirect()->back();
      }
    }
    else
    {
      toastr()->warning('You cannot start the Student Session','Warning!');
      return redirect()->back();
    }
  }

  public function student_logout($id)
  {
    if(Auth::user()->isStudent())
    {
      $client_schedule = ClientSchedule::findOrFail($id);
      $client_login = $client_schedule->client_login_time;
      $instructor_login = $client_schedule->instructor_login_time;
      $client_logout = $client_schedule->client_logout_time;
      if(!empty($client_login) && empty($client_logout))
      {
        $client_schedule->client_logout_time = Carbon::now()->format('d-m-Y H:i');
        $client_login_time = Carbon::parse($client_login);
        $client_logout_time = Carbon::parse($client_schedule->client_logout_time);
        $diff = $client_logout_time->diffInMinutes($client_login_time);
        $client_schedule->client_total_time = $diff;
        $client_schedule->update();
        toastr()->success('Student session completed Successfully, Please ask the Instructor to end the session as well','Success!');
        return redirect()->route('client-schedule.index');
      }
      elseif(!empty($client_logout))
      {
        toastr()->warning('You have already finished your session','Warning!');
        return redirect()->back();
      }
      else
      {
        toastr()->warning('Client/Instructor has not started the session yet','Warning!');
        return redirect()->back();
      }
    }
    else
    {
      toastr()->warning('You cannot finish the student session.','Warning!');
      return redirect()->back();
    }
  }
  public function instructor_logout($id)
  {
    if(Auth::user()->isAdmin() || Auth::user()->isInstructor())
    {
      $client_schedule = ClientSchedule::findOrFail($id);
      $instructor_login = $client_schedule->instructor_login_time;
      $client_login = $client_schedule->client_login_time;
      $client_logout = $client_schedule->client_logout_time;
      $instructor_logout = $client_schedule->instructor_logout_time;
      if(!empty($instructor_login) && empty($instructor_logout))
      {
        $client_schedule->instructor_logout_time = Carbon::now()->format('d-m-Y H:i');
        $instructor_login_time = Carbon::parse($instructor_login);
        $instructor_logout_time = Carbon::parse($client_schedule->instructor_logout_time);
        $difference = $instructor_logout_time->diffInMinutes($instructor_login_time);
        $client_schedule->instructor_total_time = $difference;
        $average_time = (($difference + $client_schedule->client_total_time)/2);
        $client_schedule->average_time = $average_time;
        $client_schedule->update();
        toastr()->success('Instructor has finished the sesssion successfully','Success!');
        return redirect()->route('client-schedule.index');
      }
      else
      {
        toastr()->warning('Instructor has not started the session yet','Warning!');
        return redirect()->back();
      }
    }
    else
    {
      toastr()->warning('You cannot finish the Instructor session.','Warning!');
      return redirect()->back();
    }
  }


  public function show($id)
  {
    $schedule = ClientSchedule::findOrFail($id);
    if(Auth::user()->isAdmin())
    {
      return view('client-schedule.show',compact('schedule'));
    }
    elseif(Auth::user()->isStudent())
    {
      $student_id = $schedule->client_id;
      if(Auth::id() == $student_id)
      {
        return view('client-schedule.show',compact('schedule'));
      }
      else
      {
        toastr()->warning('You cannot view other Student\'s session','Warning!');
        return redirect()->back();
      }
    }
    elseif(Auth::user()->isInstructor())
    {
      $instructor_id = $schedule->instructor_id;
      if(Auth::id() == $instructor_id)
      {
        return view('client-schedule.show',compact('schedule'));
      }
      else
      {
        toastr()->warning('You cannot view other Instructor\'s session','Warning!');
        return redirect()->back();
      }
    }
    else
    {
      toastr()->warning('You are not allowed to view Client Schedule','Warning!');
      return redirect()->back();
    }
  }
}
