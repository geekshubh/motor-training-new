<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\CarSchedule;
use App\Cars;
use Auth;
use Carbon\Carbon;
class CarScheduleController extends Controller
{
  public function index()
  {
    if(Auth::user()->isAdmin() || Auth::user()->isInstructor())
    {
      $schedule = CarSchedule::select('car_id')->distinct()->get();
      //dd($schedule);
      return view('car-schedule.index',compact('schedule'));
    }
    elseif(Auth::user()->isStudent())
    {
      $check_if_exists = CarSchedule::where('booked_by',Auth::id())->count();
      if($check_if_exists == 0)
      {
        $schedule = CarSchedule::select('car_id')->distinct()->get();
        return view('car-schedule.index',compact('schedule'));
      }
      else
      {
        toastr()->info('You have already booked your slot','Info!');
        return redirect()->route('client-schedule.index');
      }
    }
    else
    {
      toastr()->warning('You are not allowed to view Car Schedule','Warning');
      return redirect()->back();
    }
  }

  public function create()
  {
    if(Auth::user()->isAdmin())
    {
      $cars = Cars::all()->pluck('name','id');
      $availability = ['Available'=>'Available','Unavailable'=>'Unavailable'];
      return view('car-schedule.create',compact('cars','availability'));
    }
    else
    {
      toastr()->warning('You are not allowed to create Car Schedule','Warning');
      return redirect()->back();
    }
  }

  public function store(Request $request)
  {
    if(Auth::user()->isAdmin())
    {
      $schedule = New CarSchedule;
      $schedule->car_id = $request->car_id;
      $instructor_id = Cars::where('id',$request->car_id)->pluck('instructor_id')->first();
      $schedule->instructor_id = $instructor_id;
      $schedule->start_time = $request->start_time;
      $schedule->end_time = $request->end_time;
      $schedule->availability = $request->availability;
      $schedule->save();
      toastr()->success('Schedule Added Successfully','Success');
      return redirect()->route('car-schedule.index');
    }
    else
    {
      toastr()->warning('You are not allowed to create Car Schedule','Warning');
      return redirect()->back();
    }
  }

  public function edit($id)
  {
    if(Auth::user()->isAdmin())
    {
      $schedule = CarSchedule::findOrFail($id);
      $cars = Cars::all()->pluck('name','id');
      //dd($cars);
      $availability = ['Available'=>'Available','Unavailable'=>'Unavailable'];
      return view('car-schedule.edit',compact('cars','schedule','availability'));
    }
    else
    {
      toastr()->warning('You are not allowed to edit Car Schedule','Warning');
      return redirect()->back();
    }
  }

  public function update(Request $request,$id)
  {
    if(Auth::user()->isAdmin())
    {
      $schedule = CarSchedule::findOrFail($id);
      $schedule->car_id = $request->car_id;
      $instructor_id = Cars::where('id',$request->car_id)->pluck('instructor_id')->first();
      $schedule->instructor_id = $instructor_id;
      $schedule->start_time = $request->start_time;
      $schedule->end_time = $request->end_time;
      $schedule->booked_by = $request->booked_by;
      $schedule->availability = $request->availability;
      $schedule->update();
      toastr()->success('Schedule Updated Successfully','Success');
      return redirect()->route('car-schedule.index');
    }
    else
    {
      toastr()->warning('You are not allowed to update Car Schedule','Warning');
      return redirect()->back();
    }
  }

  public function destroy($id)
  {
    if(Auth::user()->isAdmin())
    {
      $schedule = CarSchedule::findOrFail($id);
      $schedule->delete();
      toastr()->success('Schedule Deleted Successfully','Success');
      return redirect()->route('car-schedule.index');
    }
    else
    {
      toastr()->warning('You are not allowed to delete Car Schedule','Warning');
      return redirect()->back();
    }
  }

  public function show($car_id)
  {
    if(Auth::user()->isAdmin() || Auth::user()->isInstructor())
    {
      $schedule = CarSchedule::where('car_id',$car_id)->get();
      $unavailable_on = Cars::where('id',$car_id)->pluck('unavailable_on')->first();
      return view('car-schedule.show',compact('schedule','unavailable_on'));
    }
    elseif(Auth::user()->isStudent())
    {
      $check_if_exists = CarSchedule::where('booked_by',Auth::id())->count();
      //dd($check_if_exists);
      if($check_if_exists == 0)
      {
        $schedule = CarSchedule::where('car_id',$car_id)->get();
        $unavailable_on = Cars::where('id',$car_id)->pluck('unavailable_on')->first();
        $car_day_off = Cars::where('id',$car_id)->pluck('unavailable_on')->first();
        $date = Carbon::now();
        $i = 0;
        $dates = [];
        while($i < 23)
        {
          $new_date = $date->addDays(1);
          $formatted_date = $new_date->format('d-m-Y');
          $check_day = $new_date->format('l');
          if($check_day != $car_day_off)
          {
            $dates[] = $formatted_date;
            $i++;
          }
        }
        //print_r($new_dates);
        return view('car-schedule.show',compact('schedule','unavailable_on','dates'));
      }
      else
      {
        toastr()->info('You have already booked your slot','Info!');
        return redirect()->back();
      }
    }
    else
    {
      toastr()->warning('You are not allowed to view Car Schedule','Warning');
      return redirect()->back();
    }
  }
}
