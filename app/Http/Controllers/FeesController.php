<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Fees;
use App\User;
use Auth;
use Illuminate\Support\Str;
use PDF;
use App\CarSchedule;
use App\Cars;
class FeesController extends Controller
{
  public function index()
  {
    if(Auth::user()->isAdmin())
    {
      $fees = Fees::all();
      return view('fees.index',compact('fees'));
    }
    elseif(Auth::user()->isStudent())
    {
      $fees = Fees::where('client_id',Auth::id())->get();
      return view('fees.index',compact('fees'));
    }
  }

  public function create()
  {
    if(Auth::user()->isAdmin())
    {
      $clients = User::where('role_id',3)->pluck('name','id');
      return view('fees.create',compact('clients'));
    }
  }

  public function store(Request $request)
  {
    if(Auth::user()->isAdmin())
    {
      $fees = new Fees;
      $fees->title = $request->title;
      $fees->installment_unique_id =  Str::uuid();
      $fees->description = $request->description;
      $fees->client_id = $request->client_id;
      $fees->amount = $request->amount;
      $fees->due_date = $request->due_date;
      $fees->status = $request->status;
      $fees->total_amount = $fees->amount;
      $fees->balance = $request->balance;
      $fees->save();
      toastr()->success('Fees Added Successfully','Success!');
      return redirect()->route('fees.index');
    }
  }

  public function edit($id)
  {
    if(Auth::user()->isAdmin())
    {
      $fees = Fees::findOrFail($id);
      $clients = User::where('role_id',3)->pluck('name','id');
      return view('fees.edit',compact('fees','clients'));
    }
  }

  public function generateInvoice($id)
  {
    $fees = Fees::findOrFail($id);
    $car_id = CarSchedule::where('booked_by',$fees->client_id)->pluck('car_id')->first();
    $car_name = Cars::where('id',$car_id)->pluck('name')->first();
    if(($fees->status == 'Paid') || ($fees->status == 'paid'))
    {
      $pdf = PDF::loadView('fees.invoice', compact('fees','car_name'));
      return $pdf->download('invoice.pdf');
    }
    else
    {
      toastr()->warning('Cannot generate Invoice for Unpaid Fees','Warning!');
      return redirect()->route('fees.index');
    }
  }

  public function update(Request $request,$id)
  {
    if(Auth::user()->isAdmin())
    {
      $fees = Fees::findOrFail($id);
      $fees->title = $request->title;
      $fees->description = $request->description;
      $fees->client_id = $request->client_id;
      $fees->amount = $request->amount;
      $fees->due_date = $request->due_date;
      $fees->total_amount = $fees->amount;
      $fees->status = $request->status;
      $fees->balance = $request->balance;
      $fees->update();
      toastr()->success('Fees Updated Successfully','Success!');
      return redirect()->route('fees.index');
    }
  }

  public function destroy($id)
  {
    if(Auth::user()->isAdmin())
    {
      $fees = Fees::findOrFail($id);
      $fees->delete();
      toastr()->success('Fees Deleted Successfully','Success!');
      return redirect()->route('fees.index');
    }
  }

  public function show($id)
  {
    if(Auth::user()->isAdmin())
    {
      $fees = Fees::findOrFail($id);
      return view('fees.show',compact('fees'));
    }
  }
}
