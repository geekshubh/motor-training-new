<?php

namespace App\Http\Controllers;

use App\Role;
use Illuminate\Http\Request;

class RolesController extends Controller
{
  public function index()
  {
    if(Auth::user()->isAdmin())
    {
      $roles = Role::all();
      return view('roles.index', compact('roles'));
    }
    else
    {
      return redirect()->back()->with('errors',trans('controller.roles.view_error'));
    }
  }

  public function create()
  {
    if(Auth::user()->isAdmin())
    {
      return view('roles.create');
    }
    else
    {
      return redirect()->back()->with('errors',trans('controller.roles.create_error'));
    }
  }

  public function store(Request $request)
  {
    if(Auth::user()->isAdmin())
    {
      Role::create($request->all());
      return redirect()->route('roles.index');
    }
    else
    {
      return redirect()->back()->with('errors',trans('controller.roles.create_error'));
    }
  }

  public function edit($id)
  {
    if(Auth::user()->isAdmin())
    {
      $role = Role::findOrFail($id);
      return view('roles.edit', compact('role'));
    }
    else
    {
      return redirect()->back()->with('errors','controller.roles.edit_error');
    }
  }

  public function update(Request $request, $id)
  {
    if(Auth::user()->isAdmin())
    {
      $role = Role::findOrFail($id);
      $role->update($request->all());
      return redirect()->route('roles.index');
    }
    else
    {
      return redirect()->back()->with('errors',trans('controller.roles.edit_error'));
    }
  }

  public function destroy($id)
  {
    if(Auth::user()->isAdmin())
    {
      $role = Role::findOrFail($id);
      $role->delete();
      return redirect()->route('roles.index');
    }
    else
    {
      return redirect()->back()->with('errors',trans('controller.roles.delete_error'));
    }
  }
}
