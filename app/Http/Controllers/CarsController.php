<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Cars;

class CarsController extends Controller
{
  public function index()
  {
    if(Auth::user()->isAdmin())
    {
      $cars = Cars::all();
      return view('cars.index',compact('cars'));
    }
    else
    {
      toastr()->warning('You are not allowed to view Cars','Warning');
      return redirect()->back();
    }
  }

  public function create()
  {
    if(Auth::user()->isAdmin())
    {
      $days = ['Monday'=>'Monday','Tuesday'=>'Tuesday','Wednesday'=>'Wednesday','Thursday'=>'Thursday','Friday'=>'Friday','Saturday'=>'Saturday','Sunday'=>'Sunday'];
      $instructors = User::where('role_id',2)->pluck('name','id');
      return view('cars.create',compact('days','instructors'));
    }
    else
    {
      toastr()->warning('You are not allowed to add Cars','Warning');
      return redirect()->back();
    }
  }

  public function store(Request $request)
  {
    if(Auth::user()->isAdmin())
    {
      $cars = New Cars;
      $cars->name = $request->name;
      $cars->unavailable_on = $request->unavailable_on;
      $cars->instructor_id = $request->instructor_id;
      $cars->save();
      toastr()->success('Car Added Successfully','Success');
      return redirect()->route('cars.index');
    }
    else
    {
      toastr()->warning('You are not allowed to add Cars','Warning');
      return redirect()->back();
    }
  }

  public function edit($id)
  {
    if(Auth::user()->isAdmin())
    {
      $cars = Cars::findOrFail($id);
      $days = ['Monday'=>'Monday','Tuesday'=>'Tuesday','Wednesday'=>'Wednesday','Thursday'=>'Thursday','Friday'=>'Friday','Saturday'=>'Saturday','Sunday'=>'Sunday'];
      $instructors = User::where('role_id',2)->pluck('name','id');
      return view('cars.edit',compact('cars','days','instructors'));
    }
    else
    {
      toastr()->warning('You are not allowed to edit Car','Warning');
      return redirect()->back();
    }
  }

  public function update(Request $request,$id)
  {
    if(Auth::user()->isAdmin())
    {
      $cars = Cars::findOrFail($id);
      $cars->name = $request->name;
      $cars->unavailable_on = $request->unavailable_on;
      $cars->instructor_id = $request->instructor_id;
      $cars->update();
      toastr()->success('Car Updated Successfully','Success');
      return redirect()->route('cars.index');
    }
    else
    {
      toastr()->warning('You are not allowed to update Car','Warning');
      return redirect()->back();
    }
  }

  public function destroy($id)
  {
    if(Auth::user()->isAdmin())
    {
      $cars = Cars::findOrFail($id);
      $cars->delete();
      toastr()->success('Car Deleted Successfully','Success');
      return redirect()->route('cars.index');
    }
  }
}
