<?php
namespace App\Http\Controllers;

use App\User;
use Auth;
use App\Role;
use Illuminate\Http\Request;
use App\Cars;
use App\ClientSchedule;
use App\CarSchedule;
use App\Fees;
class UsersController extends Controller
{
  public function index()
  {
    if(Auth::user()->isAdmin() || Auth::user()->isInstructor())
    {
      $users = User::all();
      return view('users.index', compact('users'));
    }
    else
    {
      toastr()->warning('You are not allowed to view Users','Warning');
      return redirect()->back();
    }
  }

  public function add()
  {
    if(Auth::user()->isAdmin())
    {
      $roles = Role::get()->pluck('title', 'id')->prepend('Please select', '');
      return view('users.create', compact('roles'));
    }
    else
    {
      toastr()->warning('You are not allowed to create Users','Warning');
      return redirect()->back();
    }
  }

  public function store(Request $request)
  {
    if(Auth::user()->isAdmin())
    {
      $users = new User;
      $users->name = $request->input('name');
      $users->email = $request->input('email');
      $users->password = $request->input('password');
      $users->phone_number = $request->input('phone_number');
      $users->role_id = $request->input('role_id');
      $users->ll_no = $request->input('ll_no');
      $users->ll_issue_date = $request->input('ll_issue_date');
      $users->ll_expiry_date = $request->input('ll_expiry_date');
      $users->driving_test_date = $request->input('driving_test_date');
      $users->save();
      toastr()->success('User has been added Successfully','Added');
      return redirect()->route('users.index');
    }
    else
    {
      toastr()->warning('You are not allowed to create Users','Warning');
      return redirect()->back();
    }
  }

  public function edit($id)
  {
    if(Auth::user()->isAdmin())
    {
      $roles = Role::get()->pluck('title', 'id');
      $user = User::findOrFail($id);
      return view('users.edit', compact('user','roles'));
    }
    else
    {
      toastr()->warning('You are not allowed to edit User','Warning');
      return redirect()->back();
    }
  }

  public function update(Request $request, $id)
  {
    if(Auth::user()->isAdmin())
    {
      $users = User::findOrFail($id);
      $users->name = $request->input('name');
      $users->email = $request->input('email');
      if(!empty($request->password))
      {
        $users->password = $request->input('password');
      }
      $users->phone_number = $request->input('phone_number');
      $users->role_id = $request->input('role_id');
      $users->driving_test_date = $request->input('driving_test_date');
      $users->update();
      toastr()->success('User has been updated Successfully','Updated');
      return redirect()->route('users.index');
    }
    else
    {
      toastr()->warning('You are not allowed to update User','Warning');
      return redirect()->back();
    }
  }

  public function show($id)
  {
    if(Auth::user()->isAdmin() || Auth::user()->isInstructor())
    {
      $user = User::findOrFail($id);
      $fees = Fees::where('client_id',$id)->get();
      $schedule = ClientSchedule::where('client_id',$id)->orderBy('date','Asc')->get();
      $start_date = ClientSchedule::where('client_id',$id)->orderBy('date','asc')->pluck('date')->first();
      $end_date = ClientSchedule::where('client_id',$id)->orderBy('date','desc')->pluck('date')->first();
      $completed = ClientSchedule::where('client_id',$id)->whereNotNull('average_time')->count();
      $scheduled = ClientSchedule::where('client_id',$id)->count();
      return view('users.show', compact('user','fees','start_date','end_date','completed','scheduled','schedule'));
    }
    else
    {
      return redirect()->back()->with('errors',trans('controller.users.view_error'));
    }
  }

  public function destroy($id)
  {
    if(Auth::user()->isAdmin())
    {
      $user = User::findOrFail($id);
      $user->delete();
      toastr()->success('User has been deleted Successfully','Deleted');
      return redirect()->route('users.index');
    }
    else
    {
      toastr()->warning('You are not allowed to delete User','Warning');
      return redirect()->back();
    }
  }

}
